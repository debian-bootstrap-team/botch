=head1 NAME

botch-convert-arch - convert the architecture of a Packages file

=head1 SYNOPSIS

=over

=item B<botch-convert-arch> [options] I<fromarch> I<toarch> I<inPackages> I<outPackages>

=back

=head1 DESCRIPTION

convert architecture of a Packages file from I<fromarch> to I<toarch>

=head1 OPTIONS

=over 4

=item B<-h, --help>

show help

=item B<-v, --verbose>

be verbose

=back

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

