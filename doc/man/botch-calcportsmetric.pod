=head1 NAME

botch-calcportsmetric - calculate source package importance

=head1 SYNOPSIS

=over

=item B<botch-calcportsmetric> [options] I<strongsrcgraph.xml> I<closuresrcgraph.xml>

=back

=head1 DESCRIPTION

Calculate a metric for source package importance based on how many source
packages (minimum and maximum) become compilable in a bootstrapping scenario
through the compilability of a given source package.

=head1 OPTIONS

=over 4

=item B<-h, --help>

show this help message and exit

=item B<--online>

Retrieve popcon results for source packages

=item B<-v, --verbose>

print additional information

=back

=head1 EXAMPLE

Calculate source package importance:

 zcat packages_amd64.gz | grep-dctrl -FArchitecture all > available
 botch-create_graph --deb-native-arch=amd64 --available=available --strongtype packages sources > strongbuildgraph.xml
 botch-buildgraph2srcgraph --deb-native-arch=amd64 strongbuildgraph.xml packages sources > strongsrcgraph.xml
 botch-create_graph --deb-native-arch=amd64 --available=available --closuretype packages sources > closurebuildgraph.xml
 botch-buildgraph2srcgraph --deb-native-arch=amd64 closurebuildgraph.xml packages sources > closuresrcgraph.xml
 botch-calcportsmetric --online strongsrcgraph.xml closuresrcgraph.xml > importance_metric_noall.txt

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

