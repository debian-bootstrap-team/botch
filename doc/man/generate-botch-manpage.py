#!/usr/bin/env python3

import os
import sys

tools = {
        "set operations on Packages or Sources files": [
            "botch-packages-difference",
            "botch-packages-intersection",
            "botch-packages-union",
            ],
        "transform Packages and Sources control files": [
            "botch-latest-version",
            "botch-bin2src",
            "botch-src2bin",
            "botch-clean-repository",
            "botch-add-arch",
            "botch-convert-arch",
            "botch-remove-virtual-disjunctions",
            "botch-optuniv",
            "botch-fix-cross-problems",
            "botch-filter-src-builds-for",
            ],
        "analysis of Packages and Sources control files": [
            "botch-packages-diff",
            "botch-ma-diff",
            "botch-apply-ma-diff",
            "botch-check-ma-same-versions",
            ],
        "create graphs of different types": [
            "botch-create-graph",
            "botch-annotate-strong",
            ],
        "conversion of graphs": [
            "botch-buildgraph2srcgraph",
            "botch-graphml2dot",
            "botch-collapse-srcgraph",
            "botch-profile-build-fvs",
            "botch-buildgraph2packages",
            "botch-graph-tred",
            "botch-graph2text",
            ],
        "extract regions from graphs": [
            "botch-graph-neighborhood",
            "botch-extract-scc",
            "botch-graph-ancestors",
            "botch-graph-descendants",
            "botch-graph-shortest-path",
            "botch-graph-sinks",
            "botch-graph-sources",
            ],
        "dose3 wrappers": [
            "botch-dose2html",
            "botch-buildcheck-more-problems",
            "botch-distcheck-more-problems",
            ],
        "analyze graphs": [
            "botch-calcportsmetric",
            "botch-calculate-fas",
            "botch-find-fvs",
            "botch-graph-difference",
            "botch-graph-info",
            "botch-partial-order",
            "botch-print-stats",
            "botch-multiarch-interpreter-problem",
            ],
        "create build orders": [
            "botch-build-fixpoint",
            "botch-build-order-from-zero",
            "botch-wanna-build-sortblockers",
            ],
        "handling of botch-internal formats": [
            "botch-stat-html",
            "botch-droppable-diff",
            "botch-droppable-union",
            "botch-checkfas",
            "botch-fasofstats",
            "botch-download-pkgsrc",
            ],
        "shell scripts connecting the tools for meaningful operations": [
            "botch-cross",
            "botch-native",
            "botch-transition",
            "botch-y-u-no-bootstrap",
            "botch-y-u-b-d-transitive-essential",
            ]
        }

toolnames = set([t for s in tools.values() for t in s])

scriptpath = os.path.dirname(os.path.realpath(__file__))
manpages = set([f[:-4] for f in os.listdir(scriptpath)
    if os.path.isfile(os.path.join(scriptpath,f))
    and f.startswith("botch-") and f.endswith(".pod")])

tool_but_no_man = toolnames - manpages

if tool_but_no_man:
    print("tool bat no man: %s"%tool_but_no_man, file=sys.stderr)
    exit(1)

man_but_no_tool = manpages - toolnames

if man_but_no_tool:
    print("man but no tool: %s"%man_but_no_tool, file=sys.stderr)
    exit(1)

print("""
=head1 NAME

botch - bootstrap/build order tool chain

=head1 DESCRIPTION

botch is a collection of tools to create and analyze dependency graphs. This
task is facilitated by a range of individual tools which use the deb822 and
graphml formats to exchange information with each other.

Also consider browsing the HTML based doc-base Manual at F</usr/share/doc/botch/wiki/Home.html>

The tools that botch ships allow one to:

=over 4
""")

for n,s in tools.items():
    print("""
=item %s

=over 4
"""%n)
    for t in s:
        with open(os.path.join(scriptpath,"%s.pod"%t)) as f:
            l = f.readlines()[2]
        try:
            tn, descr = l.split(" - ", 1)
        except:
            raise Exception("invalid third line format")
        if tn != t:
            raise Exception("invalid third line format")
        descr = descr.strip()
        print("""
=item B<%s>: %s
"""%(tn, descr))

    print("""
=back
""")

print("""
=back

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.
""")
