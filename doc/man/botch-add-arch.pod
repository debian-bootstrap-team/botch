=head1 NAME

botch-add-arch - add architecture to source packages

=head1 SYNOPSIS

=over

=item B<botch-add-arch> [-h] [-v] I<toarch> I<inSources> I<outSources>

=back

=head1 DESCRIPTION

Add architecture I<toarch> to the C<Architecture> field of all source packages
in I<inSources> which have an C<Architecture> field that does not match
I<toarch> yet. The system's C<dpkg-architecture> program is used to determine
whether any architecture of the source packages' C<Architecture> field matches
I<toarch>.

The files I<inSources> and I<outSources> are control files given in deb822
format like the ones found on archive mirrors and locally under
F</var/lib/apt/lists>.

I<inSources> and I<outSources> can point to the same file name for in-place
operation. Compressed I<inSources> files are decompressed on the fly
independent of their filename extension. The output will be compressed as
determined by the filename extension of I<outSources>.

If I<inSources> is equal to a single hyphen-minus (-) then its content will be
read from standard input. If I<outSources> is equal to a single hyphen-minus
(-) then the output will be written to standard output.

=head1 OPTIONS

=over 4

=item B<-v, --verbose>

Be verbose.

=item B<-h, --help>

Print help message.

=back

=head1 EXAMPLE

Make all source packages compilable on I<arm64>:

 botch-add-arch arm64 inSources outSources

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

