=head1 NAME

botch-packages-diff - create a diff between two Packages or Sources files

=head1 SYNOPSIS

=over

=item B<botch-packages-diff> [options] I<inPackages1> I<inPackages2>

=back

=head1 DESCRIPTION

print the binary packages whose fields differ between two packages files.

=head1 OPTIONS

=over 4

=item B<--verbose>

Be verbose.

=item B<-h, --help>

Display help.

=back

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

