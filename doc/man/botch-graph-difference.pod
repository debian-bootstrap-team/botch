=head1 NAME

botch-graph-difference - graph diff

=head1 SYNOPSIS

=over

=item B<botch-graph-difference> [options] I<g.xml> I<h.xml>

=back

=head1 DESCRIPTION

Find if two GraphML or dot files differ and show the difference.

=head1 OPTIONS

=over 4

=item B<-h, --help>

Show help.

=item B<-v, --verbose>

Be verbose.

=back

=head1 EXIT STATUS

If the files differ, the exit status is non-zero, otherwise it is zero.

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

