\documentclass{beamer}
\usetheme[secheader]{Boadilla}

\title[Bootstrapping Debian]{Bootstrapping Debian (or derivatives) for a new architecture}
\author[J. Schauer]{Johannes Schauer}
\institute[JUB]{Jacobs University Bremen}
\date{20-09-2012}

\begin{document}

\frame{\titlepage}

\section{Introduction}

\frame{
	\frametitle{Overview}
	\begin{itemize}
		\item Google Summer of Code 2012 project
		\item Mentored by wookey and Pietro Abate
		\item written in OCaml and Python
		\item using dose3
		\item released under GNU LGPL3+
		\item \url{https://gitorious.org/debian-bootstrap/bootstrap}
		\item IRC: \texttt{\#debian-bootstrap @ irc.debian.org}
		\item mailing list: \texttt{\href{mailto:debian-bootstrap@lists.mister-muffin.de}{debian-bootstrap@lists.mister-muffin.de}}
		\item please ask questions any time
	\end{itemize}
}

\frame{
	\frametitle{Motivation}
	\begin{itemize}
		\item Debian packages are neither made to be cross compilable nor to be built without an existing full Debian (no stage builds)
		\item but for each new port a set of source packages has to be cross compiled and/or built with reduced build dependencies
		\item because there is no official procedure for above tasks, the process is long and manual, involves foreign distributions, manual hacking of packages
		\item Debian was ported to more than 20 architectures so the process is executed roughly once per year
	\end{itemize}
}

\frame{
	\frametitle{Motivation (cont.)}
	\begin{itemize}
		\item porting Debian to a new architecture would less time consuming and less problematic
		\item remove the need of other distributions during porting, make Debian more universal as it will then be able to build itself from scratch
		\item update lagging architectures
		\item build Debian for architectures that cannot build themselves (avr32)
		\item make sub-arch builds optimized for a specific CPU easier
	\end{itemize}
}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

% * overview
%    - find minimal set of source packages that have to be cross compiled
%    - analyze build dependency cycles
%    - create a build order
%    - results
%    - future

\section{Find source packages that must be cross compiled}

\frame{
	\frametitle{Cross compiling the initial set of packages}
	\begin{itemize}
		\item preference of native compilation over cross compilation
		\item when starting with nothing a minimal build system has to be cross compiled
	\end{itemize}
}

\frame{
	\frametitle{Finding the minimal set of source packages that must be cross compiled}
	\begin{enumerate}
		\item find binary packages that are either:
			\begin{itemize}
				\item \texttt{Essential:Yes}
				\item \texttt{Build-Essential:yes}
				\item \texttt{Priority:Required}
				\item \texttt{Package:build-essential}
				\item most likely also debhelper as 79\% of the archive depend on it
			\end{itemize}
		\item get their installation set
		\item find the source packages that build the installation set excluding \texttt{Architecture:all} packages
	\end{enumerate}
}

\section{Native build dependency analysis}

\frame{
	\frametitle{Start building natively}
	\begin{enumerate}
		\item based upon minimal build system
		\item install as many binary packages as possible
			\begin{itemize}
				\item all \texttt{Architecture:all} packages
				\item binary packages that were cross compiled
				\item binary packages that were natively compiled in earlier iterations
			\end{itemize}
		\item build as many source packages as possible, if some new source packages could be built, go to 2
		\item investigate situation
	\end{enumerate}
}

\frame{
	\frametitle{Investigate build dependency cycle situation}
	\begin{itemize}
		\item if there are still packages left after the algorithm finishes, then there are circular build dependencies
		\item try to solve circular dependency situation by looking closer at
			\begin{itemize}
				\item binary packages needed as build dependency by most source packages
				\item smallest dependency cycles
				\item source packages with least build dependencies missing
				\item source packages with only "weak" build dependencies missing
			\end{itemize}
	\end{itemize}
}

\frame{
	\frametitle{Availability of binary packages}
	\begin{itemize}
		\item if a binary package is not yet available in the new system, then this is because of one of the following reasons:
			\begin{itemize}
				\item its source package cannot be built
				\item binary packages from its installation set are not available
				\item both of the above
			\end{itemize}
		\item a binary package can be made available by:
			\begin{itemize}
				\item cross compiling the source packages(s) that provide it and/or its installation set
				\item introducing reduced build dependencies
				\item moving build dependencies into \texttt{Build-Depends-Indep}
				\item a combination of the above
			\end{itemize}
	\end{itemize}

}

\frame{
	\frametitle{Making a package available through cross compilation}
	\begin{enumerate}
		\item find the installation set of the selected binary package
		\item remove all binary packages that are already available
		\item find the source packages that build the rest
	\end{enumerate}
	\begin{itemize}
		\item but native compilation is preferred
		\item but also blocked by build dependency cycles
		\item for finding reduced build dependencies that break the build dependency cycles, the dependency graph has to be built
	\end{itemize}
}

\frame{
	\frametitle{The dependency graph}
	\begin{itemize}
		\item the dependency graph has two types of vertices
			\begin{itemize}
				\item source package nodes
				\item binary packages and their installation set
			\end{itemize}
		\item the dependency graph has two types of edges
			\begin{itemize}
				\item build dependency (source $\rightarrow$ binary)
				\item builds-from (binary $\rightarrow$ source)
			\end{itemize}
	\end{itemize}
}

\frame{
	\frametitle{The dependency graph (cont.)}
	\begin{itemize}
		\item graph is built by recursively adding nodes and edges, starting from the root node that is the investigated package
		\item source package nodes connect to all binary package node that represent their build dependencies
			\begin{itemize}
				\item except for binary packages that are already installable
			\end{itemize}
		\item binary packages (and their installation set) connect to all source package nodes that they build from
			\begin{itemize}
				\item installation set excludes binary packages that are already available
					\begin{itemize}
						\item natively built
						\item cross built
						\item \texttt{Architecture:all}
					\end{itemize}
			\end{itemize}
	\end{itemize}
}

\frame{
	\frametitle{Possibilities of dependency graph analysis}
	\begin{itemize}
		\item show statistics
			\begin{itemize}
				\item binary/source nodes with most/least incoming/outgoing edges
				\item most/lesat connected nodes
				\item highest/lowest ratios
			\end{itemize}
		\item show/save DOT graph
		\item show cycles up to length N (N is even)
	\end{itemize}
}

\frame{
	\frametitle{Possibilities of dependency graph analysis (cont.)}
	\begin{itemize}
		\item source packages only missing a few build dependencies
		\item binary packages with highest ratio of source packages it needs to be built and source packages that build depend on it
			\includegraphics[width=\linewidth]{reduced_deps_1.png}
		\item source packages with highest ratio of build dependencies and source packages that build-depend on packages that depend on it
			\includegraphics[width=\linewidth]{reduced_deps_2.png}
	\end{itemize}
}

\frame{
	\frametitle{Enumerating elementary circuits of a directed graph}
	\begin{itemize}
		\item using Johnson's algorithm (1975)
		\item lack of existing, well-tested cycle enumeration code
		\item validated by comparing output with implementations of Johnsons's algorithm in Java, Tarjan's algorithm in Python and Hawick and James' algorithm in D
		\item added functionality to only enumerate cycles up to a certain length and amount
		\item code at \url{https://github.com/josch/cycle\_test}
	\end{itemize}
}

\frame{
	\frametitle{Distribution graphs}
	\begin{itemize}
		\item observation: most packages are involved in the same SCC
		\item do not build a graph starting from package X but for a whole distribution by same principles as above
		\item finding cycles in the resulting graph takes long (4 hours on 2.5GHz Core i5, 700MB memory consumption)
		\item many packages exist in the graph that are not part of the "core" packages but are just leave nodes
	\end{itemize}
}

\frame{
	\frametitle{Reduced distribution}
	\begin{itemize}
		\item idea: start analysis on a "reduced distribution" and continue from there
		\item what is a reduced distribution:
			\begin{itemize}
				\item contains a set of source packages A and a set of binary packages B
				\item all binary packages in B except for \texttt{Architecture:all} packages can be built from the source packages in A
				\item all source packages in A are buildable with the binary packages in B
			\end{itemize}
		\item analysis of a reduced distribution only takes minutes and results were shown to be the same as with a full distribution
	\end{itemize}
}

\frame{
	\frametitle{Reduced distribution (cont.)}
	\begin{itemize}
		\item how the package selection is made:
			\begin{enumerate}
				\item start with initial selection of binary packages (eg: essential plus build-essential)
				\item find source packages that build those binary packages
				\item find binary packages that are necessary to build those source packages, if those are more than durign the last iteration, go to 2.
				\item done
			\end{enumerate}
	\end{itemize}
}

\section{Deducing a build order}

\frame{
	\frametitle{Deducing a build order}
	\begin{enumerate}
		\item starting from zero
		\item cross compile all source packages from the minimal set of packages that can be cross compiled, if done, go to 4.
		\item use reduced build dependencies to solve a dependency cycle and go to 2.
		\item switch to native compilation
		\item build all binary packages that can be built, if all binary package can be built, go to 7.
		\item use reduced build dependencies and cross compilation to solve a dependency cycle and go to 5.
		\item done
	\end{enumerate}
}

\frame{
	\frametitle{Deducing a build order (cont.)}
	\begin{itemize}
		\item not possible yet because of:
			\begin{itemize}
				\item unsatisfied cross build dependencies because of missing multiarch annotation
				\item insufficient number of reduced build dependencies to solve dependency cycles
			\end{itemize}
		\item what is blocking the above:
			\begin{itemize}
				\item wanna-build doesnt support architecture qualifiers (\texttt{pkg:any}, \texttt{pkg:native}, \texttt{pkg:amd64}, ...)
				\item no decision on format of reduced build dependencies
			\end{itemize}
		\item after both issues are solved, changes have to be implemented into actual packages
	\end{itemize}
}

\frame{
	\frametitle{Build profiles}
	\begin{itemize}
		\item current proposal for reduced build dependencies: build profiles
		\item \texttt{Build-Depends: foo [i386 arm] <!stage1 embedded>}
		\item format similar to architecture specifiers
		\item has other uses like building a source package for embedded architectures or without docs
		\item trivial to implement in dpkg and dose3 (patches available for both)
		\item no duplication of build dependency list (less bitrot)
	\end{itemize}
}

\section{Results}

\frame{
	\frametitle{Results}
	\begin{itemize}
		\item actual output (package listings etc) can be seen at \url{http://wiki.debian.org/DebianBootstrap/TODO}
		\item list of source packages for a minimal build system
		\item list of source packages with their unsatisfied cross build dependencies
		\item packages depending on \texttt{Multi-Arch:foreign} packages without \texttt{:any} qualifier
		\item list of packages that are \texttt{Multi-Arch:none} in Debian but not in Ubuntu
		\item packages that build \texttt{Architecture:all} packages but have no \texttt{Build-Depends-Indep} field but a \texttt{binary-indep} and/or \texttt{build-indep} target
	\end{itemize}
}

\frame{
	\frametitle{Results (cont.)}
	\begin{itemize}
		\item list of source packages that are part of the main scc
			\begin{itemize}
				\item the dependency graph generated for Debian Sid has 39486 vertices
				\item it has only one central scc with 1027 vertices
				\item eight other scc with 2 to 7 vertices
				\item contains not-nice packages like: nautilus, iceweasel, metacity, evolution...
			\end{itemize}
	\end{itemize}
}

\frame{
	\frametitle{Results (cont.)}
	\begin{itemize}
		\item list of source packages only missing a few build dependencies
		\item list of source packages that build-depend on many others but are only needed by few binary packages which are in turn only needed buy a few source packages
		\item list of source packages that are only needed by few source package but need many other source packages to be built to satisfy their runtime dependencies
		\item list of dependency cycles of length 2 and 4
			\begin{itemize}
				\item example of cycles of length 2 (source package build depends on the binary package it builds): vala, python, mlton, fpc, sbcl, ghc
				\item no more dependency cycles because there would be 958 cycles of length 6, 5566 cycles of length 8 and 37839 cycles of length 10
			\end{itemize}
	\end{itemize}
}

\frame{
	\frametitle{Minimal build system}
\begin{tabular}{lll}
                             & Debian Sid & Ubuntu Precise \\ \hline
\texttt{priority:required}   & 37         & 70 \\
\texttt{essential:true}      & 25         & 24 \\
\texttt{buildessential:true} & 11         & 44 \\
the above plus dependencies  & 106        & 140 \\
number of source packages    & 55         & 75 \\ \hline
\end{tabular}
}

\frame{
	\frametitle{Reduced distribution}
\begin{tabular}{lll}
                                  & Debian Sid  & Ubuntu Precise \\ \hline
src/bin in original repositories  & 18266/37781 & 3305/8076 \\
src/bin without important         & 645/2324    & 522/1838 \\
src/bin with important            & 679/2403    & 541/1871 \\
src/bin imp. + task-gnome-desktop & 855/2853    & - \\
src/bin imp. + ubuntu-desktop     & -           & 718/2467 \\
src/bin imp. + task-kde-desktop   & 791/2769    & - \\
src/bin imp. + kubuntu-desktop    & -           & 618/2158 \\ \hline
\end{tabular}
}

\frame{
	\frametitle{Cross everything}
	\begin{itemize}
		\item if there were no reduced build dependencies but only cross compilation, crosscompiling the following number of packages would make the whole archive available
	\end{itemize}
\begin{tabular}{llll}
         & task-gnome-desktop & Precise & ubuntu-desktop \\ \hline
to cross & 158                & 176     & 157 \\ \hline
\end{tabular}
	\begin{itemize}
		\item it cannot yet be shown that reduced build dependencies are unavoidable
		\item they are unavoidable if they are needed to build the minimal set of packages
	\end{itemize}
}

\section{Future}

\frame{
	\frametitle{Future}
	\begin{itemize}
		\item allow to analyze the dependency graph for cross building
		\item find more clever methods to break the main scc
		\item combine everything for a final buildorder
		\item close work with wookey to make base packages crosscompile
		\item work is done based on Ubuntu because of wanna-build blocker
		\item make use of Gentoo USE flags
	\end{itemize}
}

\section{Questions}

\frame{
	\frametitle{Questions?}
}

\end{document}
