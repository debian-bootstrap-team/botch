\documentclass{beamer}
\usetheme[secheader]{Boadilla}
\usepackage{graphicx}
\usepackage[noend]{algpseudocode}
\usepackage{url}
%\usepackage{datatool}

\title[Bootstrapping Debian]{Bootstrapping Debian for a new architecture}
\author[P. Abate]{Pietro Abate \\ Johannes Schauer}
\institute[P7/Irill/Inria]{Universite Paris Diderot / Irill}
\date{Fossa 2012, Lille - 04-12-2012}

\begin{document}

\frame{\titlepage}

\section{Introduction}

\frame{
  \frametitle{Problem}
  \begin{itemize}
    \item Debian was ported to more than 20 architectures so the 
      process is executed roughly once per year
    \item Debian packages are neither made to be cross compilable nor
      to be built without an existing full Debian installation
    \item For each new port a \texttt{set} of source packages has to 
      be cross compiled and/or built manually
    \item Bootstrap a new architecture often involves foreign 
      distributions and a lot of hacking
  \end{itemize}
}

\frame{ 
  \frametitle{Wish List}
  \begin{itemize}
    \item Porting Debian to a new architecture should be less time 
      consuming and less problematic.
    \item No foreign distributions during porting (self hosted).
    \item Full automatic cross compiling for architectures that cannot build 
      themselves.
    \item Sub-arch builds optimized for a specific CPU should be
      easier.
  \end{itemize}
}

\frame{         
  \frametitle{The final Goal : Deducing a build order}
  \begin{enumerate}
    \item Step zero : Bare metal.
    \item Cross compilation : create a minimal build system ($XC$).
    \item Automatic (cross) compile $XC$.
    \item Switch to native compilation.
    \item Find the largest number of sources that can be natively
      built ($NC$).
    \item Automatic compile $NC$ (we need a build order).
  \end{enumerate}
}

\begin{frame}
  \frametitle{The proposed pipeline}
  \begin{itemize}
    \item Generate a description of the packages metadata for the new
      architecture.
    \item Select a set of binary packages that will constitute the
      minimal build system and compute an installation set for it.
    \item Compute the set of source packages need to cross build the
      minimal build system and identify those packages that can be
      cross built or those who cannot. 
    \item Cross compile and install the minimal build system on the new
      architecture.
    \item Identify the maximal set of packages that can be native 
      compiled using binary packages in the minimal build system.
    \item Generate a dependency build graph, remove cycles and compute
      a build order.
    \item Build all packages natively.
  \end{itemize}
\end{frame}

%\frame{
%  \frametitle{Why we need Cross Compilation ?}
%  \begin{itemize}
%    \item A new architecture cannot be bootstrapped from thin air.
%    \item At least a minimal system must be cross built.
%    \item This system should be large enough to compile the entire
%      distribution.
%    \item Native compilation should be preferred over cross
%      compilation as it allows to uncover architecture specific
%      problems.
%  \end{itemize}
%}

\begin{frame}
  \frametitle{Stage Compilation}
  \begin{itemize}
    \item Directly building fully fledged binary packages is
      impossible because of the presence of \texttt{build dependency
      cycles}
    \item We need to weak build dependencies in order to remove these 
      dependency cycles.
    \item \texttt{Build Profiles} are the proposed solution.
      \begin{itemize}
        \item A build profile is a global build dependency filter
        \item It is the form : \texttt{Build-Depends: foo [i386 arm] <!stage1>}
        \item The format similar to architecture specifiers
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Stage Compilation}
  \includegraphics[width=\linewidth]{StageCompilation.png}
\end{frame}

\section{Bootstrap for a new architecture}

\frame{
  \frametitle{Cross compilation. Package selection.}

    The minimal set of packages that must be cross compiled ($XC$) are 
    those with the following properties :

    \begin{itemize}
      \item \texttt{Essential: yes}
      \item \texttt{Build-Essential: yes}
      \item \texttt{Priority: required}
      \item \texttt{Package: build-essential}
    \end{itemize}

    Plus debhelper as 79\% of the archive depend on it
}

\begin{frame}[fragile]
  \frametitle{Cross compilation. Package selection.}

Reusing existing tools !
\begin{verbatim}
bzcat Quantal-i386-Packages.bz2 | \
grep-dctrl -FPackage build-essential --or \
           -FPackage debhelper --or \
           -Fpriority required --or \
           -FEssential yes > minimal-i386
\end{verbatim}

And new ones :
\begin{verbatim}
deb-coinstall --bg Quantal-i386-Packages.bz2 
              --fg minimal-i386 \
              --src Quantal-Sources.bz2 > minimal-i386-src
\end{verbatim}
\end{frame}

\frame{
  \frametitle{Minimal build system}
  \begin{itemize}
    \item How many packages are in the minimal build system for Sid ?

      \begin{tabular}{lll}         
                                      & Debian Sid & Ubuntu Precise \\ \hline
        \texttt{Priority: required}   & 37         & 70 \\
        \texttt{Essential: yes}       & 25         & 24 \\
        \texttt{Build-Essential: yes} & 11         & 44 \\
        how many bianry packages      & 106        & 140 \\
        how many source packages      & 55         & 75 \\ \hline
      \end{tabular}
    
    \item Many packages in $XC$ would cross-build just fine if their
    cross-build-dependencies could be resolved using Multi-Arch.
    \item \texttt{Challenge:} Automatically Cross compile the minimal build
    system.
  \end{itemize}
}

\frame{
  \frametitle{Multi Architecture}
  \begin{itemize}
    \item It is a Debian specific feature.
    \item It is used to use a architecture specific package 
      (under certain circumstances) on other architectures.
    \item It is expressed as additional package meta data and
      needs to be added manually by the package maintainer.
    \item In out context, adding multi-arch annotations will allow 
      packages to be cross built on a different architecture of their
      own.
  \end{itemize}
}

\begin{frame}[fragile]
\small
  \frametitle{Test cross-build-dependency resolution}
  With \texttt{apt-get} (adding an armel as foreign architecture):
  \begin{verbatim}
apt-get --simulate --host-architecture=armel build-dep <package>
  \end{verbatim}

  Or with \texttt{dose-buildebcheck} (static check):
  \begin{verbatim}
dose-builddebcheck --success --failures --explain \
 --deb-native-arch=amd64 \
 --deb-host-arch=armhf \
 ubuntu_dists_quantal_main_binary-amd64_Packages \
 ubuntu_dists_quantal_main_source_Sources
  \end{verbatim}
\end{frame}

\frame{
  \frametitle{We can't cross compile the minimal build system (yet !)}
  \includegraphics[width=\linewidth]{xcompile.png}
}


\frame{
  \frametitle{Which packages can be natively compiled from $XC$ ?}
  Maximal set of source package that can be compiled natively.
  \begin{itemize}
    \item $B_i$ : set of binary
      packages that are currently available.
    \item $S$ : set of packages that we want to compile.
    \item $S_i$ : set of source packages that can be successfully compiled.
  \end{itemize}
  \begin{figure}[t]
    \begin{algorithmic}[1]
      \Procedure{Build}{$S_i, B_i, S$}
        \State $\mathtt{S_{i+1}} \gets find\_installable (B_i,S)$
        \If {$\mathtt{S_{i+1}} = \emptyset$}
          \State \textbf{return} {$S_i$}
        \Else
         \State $\mathtt{B_{i+1}} \leftarrow Bin(S_{i+1}) \cup B_i$
         \State \textbf{return} {${\tt BUILD} (S_i \cup S_{i+1}, B_{i+1}, S \setminus S_{i+1})$}
        \EndIf
      \EndProcedure
      \State ${\tt ALL\_NATIVE} \gets {\tt BUILD} (\emptyset,Bin(XC),S)$
    \end{algorithmic}
  \end{figure}
}

\section{Results}

\frame{
  \frametitle{The dependency graph}
  \includegraphics[width=\linewidth]{buildgraph.png}
  \begin{itemize}
    \item Two types of vertex
      \begin{itemize}
        \item source packages.
        \item build-dependency set (binaries needed to build a source
          package)
      \end{itemize}
    \item Two types of edges
      \begin{itemize}
        \item build-dep (source $\rightarrow$ binary)
        \item built-from (binary $\rightarrow$ source)
      \end{itemize}
  \end{itemize}
  \begin{itemize}
    \item Built iteratively by adding connecting each source package
      to the set of its build dependencies and each build dependencies
      set to all source packages whose binaries are build from.
    \item Packages that are cross-built ($p \in XC$) or with
      \texttt{Architecture:all} are excluded from the dependency
      graph.
  \end{itemize}
}

\frame{
  \frametitle{Some numbers on the build graph}
    \begin{itemize}
      \item the dependency graph generated for Debian Sid has 
        39486 vertices.
      \item it has only one central SCC with 1027 vertices.
      \item eight other SCC with 2 to 7 vertices.
      \item contains not-nice packages like: nautilus, iceweasel, 
        metacity, evolution, etc
      \item contains many build dependency cycles.
      \item \texttt{Challenge:} (Automatically) Simplify the Build
        Dependency Graph.
    \end{itemize}
}

\begin{frame}
  \frametitle{Simplify the Build Dependency Graph.}
  \begin{itemize}
    \item Remove architecture independent dependencies :
      \begin{itemize}
        \item The control fields \texttt{Build-Depends-Indep} and
          \texttt{Build-Conflicts-Indep} identify dependencies or
          conflicts for building architecture:all packages
        \item We are not interested to build architecture:all packages
          therefore we can drop \texttt{Build-Depends-Indep} and
          \texttt{Build-Conflicts-Indep} dependencies
      \end{itemize}
    \item Find Weak dependencies :
      \begin{itemize}
        \item Manually identify packages that are not strictly needed
          to compile a working, albeit not fully fledged, package.
        \item Use external information to identify weak build dependencies
          (gentoo compile flags)
        \item Add \textbf{build profiles} (ex. stage1, embedded,
          nodoc, etc) to source packages.
      \end{itemize} 
  \end{itemize}
\end{frame} 

\frame{
  \frametitle{Dependency graph analysis}
  We can easily identify :
  \begin{itemize}
    \item binary/source nodes with most/least incoming/outgoing edges
    \item most/least connected nodes
%    \item highest/lowest ratios
    \item source packages only missing a few build dependencies
    \item binary packages with highest ratio of source packages it 
        needs to be built and source packages that build depend on it
        \includegraphics[width=\linewidth]{reduced_deps_1.png}
    \item source packages with highest ratio of build dependencies and 
        source packages that build-depend on packages that depend on it
        \includegraphics[width=\linewidth]{reduced_deps_2.png}
  \end{itemize}
}               

\frame{                 
  \frametitle{Current Unresolved Issue in Debian}
  \begin{itemize}
    \item Provide a build order is still difficult because :
      \begin{itemize}
        \item unsatisfied cross build dependencies because of missing 
          multi-arch annotation
        \item insufficient number of reduced build dependencies to solve 
          dependency cycles
      \end{itemize}
    \item A discussion is on going in the Debian community to agree on
      the best way to implement the necessary changes.
%    \item what is blocking the above:
%      \begin{itemize} 
%        \item wanna-build doesn't support architecture qualifiers 
%          (\texttt{pkg:any}, \texttt{pkg:native}, \texttt{pkg:amd64}, ...)
%        \item no decision on format of reduced build dependencies
%      \end{itemize}
%    \item after both issues are solved, changes have to be manually 
%      implemented into actual packages
  \end{itemize}
}

\section{Conclusions}

\begin{frame}
  \frametitle{Future work}
  \begin{itemize}
    \item Identify a list of plausible weak dependencies (Work in
      progress to use Gentoo build-flags)
    \item Devise an algorithm to automatically break build cycles
      using weak dependencies (almost done)
    \item Create a topological sort of the build dependency graph
      (almost done)
    \item Provide a build order to be used to bootstrap debian of a
      foreign architecture.
    \item Generalize this solution to a larger class of problems.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Tools and Resources}
  All our tools and experiments are available :
  \begin{itemize}
    \item Debian Bootstrap :
      \url{https://gitorious.org/debian-bootstrap/bootstrap}
    \item Dose : \url{https://gforge.inria.fr/projects/dose/}
    \item dose-builddebcheck :
      \url{http://packages.debian.org/wheezy/dose-builddebcheck}
  \end{itemize}

  \begin{itemize}
    \item Main page : \url{http://wiki.debian.org/DebianBootstrap}
    \item Lots of details :
      \url{http://wiki.debian.org/DebianBootstrap/TODO}
    \item Multi-Arch Cross spec
      \url{https://wiki.ubuntu.com/MultiarchCross}
    \item Multi-Arch spec : \url{https://wiki.ubuntu.com/MultiarchSpec}
    \item Linaro Cross Compile Howto
      \url{https://wiki.linaro.org/Platform/DevPlatform/CrossCompile/UsingMultiArch}
  \end{itemize}
\end{frame}

\end{document}
