\documentclass{beamer}
\usetheme[secheader]{Boadilla}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[noend]{algpseudocode}
\usepackage{url}

\newcommand{\power}[1]{\mathcal{P}(#1)}

\title[Bootstrapping Software Distributions]{Bootstrapping Software Distributions}

\author[P. Abate, J. Schauer]{Pietro Abate \inst{1} \and Johannes Schauer \inst{2}}

\institute[Diderot, Jacobs]
{
  \inst{1}
  Univ Paris Diderot, PPS, \\
  UMR 7126, Paris, France
  \and
  \inst{2}
  Jacobs University Bremen, \\
  College Ring 3, MB670, \\
  28759 Bremen
}

\date{CBSE 2013}

%\AtBeginSection[]
%{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection]
%  \end{frame}
%}

\begin{document}

\frame{\titlepage}

%\begin{frame}{Outline}
%  \tableofcontents
%\end{frame}

\section{Motivation}

\subsection{Problem Description}

\begin{frame}{The Bootstrap Problem}
  \begin{itemize}
    \item {\bf Software distributions} are composed of thousands of
      components, interconnected by a web of dependency and
      conflict relations among {\bf binary packages}.
    \item FOSS distributions are available for a number of
      different {\bf hardware architectures} with new architectures
      added every year.
    \item Interdependencies between {\bf source packages} give
      rise to millions of circular dependencies in form of {\bf Strongly
      Connected Components}
    \item {\bf Porting} a distribution to a new architecture means to
      recompile all source packages for that new architecture.
    \item The goal of this work is to
      provide tools and heuristics to {\bf remove} all build
      dependency cycles with a {\bf minimal} amount of changes and to deduce a
      {\bf build order}
  \end{itemize}
\end{frame}

\begin{frame}{Status Quo}
  \begin{itemize}
    \item Porting a distribution to a new hardware platform was not an
      automatic process.
    \item Build cyclic dependencies had to be found manually without a
      clear method (mostly handicraft work).
    \item Removing cyclic build dependencies likely entailed more changes
      than necessary.
    \item Poor documentation: the process had to be repeated for every 
      port.
    \item Process took up to a year.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{figure}
    \includegraphics[width=\textwidth]{dep_graph_scc_2013}
  \end{figure}
\end{frame}

%% Before adding this slide you should explain what is a SCC in this
%% context.
\begin{frame}{Biggest SCC January 2013}
  \begin{figure}
    \center\includegraphics[width=.7\textwidth]{hairball}
  \end{figure}
\end{frame}

\subsection{Our Contribution}

\begin{frame}{Our Contribution}
  \begin{itemize}
    \item Provide a formal framework to reason about build
      dependencies.
    \item Develop algorithms to untangle the dependency
      graph and heuristic to remove build cycles.
    \item Calculate a build order with minimal changes to packages
    \item Test our tools on the Debian software distribution and
      create a liaison with the Debian community. 
  \end{itemize}
\end{frame}

% usually not needed ..
%\subsection{Related Work}
%
%\begin{frame}{Related Work}
%  \begin{itemize}
%    \item Explain which existing research in this area exists
%    \item And why it doesnt solve the problem
%    \item Show how they also discovered that the problem cannot be fully automated
%  \end{itemize}
%\end{frame}

\section{Terminology}

%% you can use the figure 3 in the paper to explain this\ldots
\begin{frame}{Binary and Source Packages}
  \begin{itemize}
    \item \textbf{Source Packages}
      \begin{itemize}
        \item Project source code and metadata
        \item Build-Depends on binary packages
        \item Build-Conflicts with binary packages
        \item Builds binary packages
      \end{itemize}
    \item \textbf{Binary Packages}
      \begin{itemize}
        \item Executables, data files, metadata
        \item Depends on other binary packages
        \item Conflicts with other binary packages
        \item Builds from source packages
      \end{itemize}
  \end{itemize}
\end{frame}

%% for the following two slides I will just explain everything verbally so I
%% didnt add slides saying everything in text form
\begin{frame}{Dependency Graph}
  \begin{figure}
    \includegraphics[width=\textwidth]{{runningex.dot}.eps}
  \end{figure}
\end{frame}

\begin{frame}{Installation Sets}
  \begin{itemize}
    \item A set of packages for which for each package
      \begin{itemize}
        \item All dependencies are satisfied
        \item No conflicting package is part of the set
      \end{itemize}
    \item Due to disjunctive dependencies binary packages can have
      multiple installation sets 
    \item We do not treat binary packages individually but always
      together with an installation set
  \end{itemize}
\end{frame}

\begin{frame}{Build Graph and Source Graph}
  \begin{figure}
    \includegraphics[scale=0.4]{{buildgraph.dot}.eps}
    \qquad
    \includegraphics[scale=0.4]{{sourcegraph.dot}.eps}
  \end{figure}
\end{frame}

%% maybe add a slide with the algorithm and an example to explain it
%% I think the algorithm is very straight forward and I can explain it while
%% just having the graphs up, no?

%% I didnt add the math to the slides because I think during a presentation
%% it's easier to explain it in plain english - do you agree?

%% XXX On the other end these people are mathematicians, and the love to have 
%% formal explanation 
\begin{frame}{Repositories}
  A repository is a tuple $(P,Dep,Con,Bin)$ where
  \begin{itemize}
    \item $P$ is a set of binary or source packages
    \item $Dep$ is the dependency function (builddep or depends)
    \item $Con$ is the conflict relation toward binary packages
    \item $Bin$ is the binary function mapping source packages to the binary
      packages they build
  \end{itemize}
\end{frame}

\begin{frame}{Build Profiles}
  \begin{itemize}
    \item A \textbf{Build profiles} is a different configuration for
      compiling a source package
    \item It is used to remove dependency cycles by building
      source packages with a reduced feature set and therefore less build
      dependencies
    \item Examples:
      \begin{itemize}
        \item no documentation
        \item no language bindings
        \item optional features
        \item disable unit tests
      \end{itemize}
    \item Record changes in source package metadata
    \item The function $Pmap$ transforms repositories into repositories
      where some of the source packages were transformed using Build profiles.
  \end{itemize}
\end{frame}

% add the definition of build problem and the formal problem statement 
%% I changed "Installable" to "Compilable" to avoid confusion
%% I fixed a typo we have in the paper "Pmap_1" -> "Pmap_2"
%% I fixed a typo we have in the paper "S" -> "S_n"
%% I fixed a typo we have in the paper "R_1" -> "R_2"
%% I added the union the binary packages of the minimal build system as those
%% binary packages will not all be compiled after the first step
%% The binary packages of the minimal build system are not added at the last
%% step to indicate that at the very least in the last step they are all compiled
%% meh... I wish I'd seen all those tiny bits earlier... :(
\begin{frame}{The Bootstrap Problem}

  \begin{itemize}
    \item $R$ is the initial repository.
    \item $B_0$ is the minimal build system.
    \item $B$ is the final binary repository.
    \item $Src(R)$ retrieves all source packages from a repository $R$.
  \end{itemize}
\[
  \begin{array}{rl}
    R_1 &= Pmap_1(R) \\
    S_1 &= Src(R_1) \\
    B_1 &= Bin ( Compilable(S_1,B_0) ) \cup B_0 \\
    \\
    R_2 &= Pmap_2(R_1) \\
    S_2 &= Src(R_2) \\
    B_2 &= Bin ( Compilable(S_2,B_1) ) \cup B_0 \\
    \cdots \\
    R_n &= Pmap_n(R_{n-1}) \\
    S_n &= Src(R_n) \\
    B_n &= Bin ( Compilable(S_n,B_{n-1}) ) = B \\
  \end{array}
\]
\end{frame}

\begin{frame}{Bootstrap Workflow}
  \begin{enumerate}
    \item Select binary packages for minimal build system and cross compile
      them for the new platform
    \item Build all source packages that can be built without breaking
      cycles (fixpoint algorithm in the paper)
    \item Create Build Graph and extract SCC
    \item If the amount of existing build profiles is not enough to make
      build graph acyclic:
      \begin{itemize}
        \item use heuristics to find source packages to add build profiles
          to
        \item modify the respective source packages
        \item go back to 3
      \end{itemize}
    \item Feedback Vertex Set algorithm selects source packages to profile
      build and a build order is deduced from the now acyclic graph
  \end{enumerate}
\end{frame}

\section{Approach}

\subsection{Heuristics}

\begin{frame}{Heuristics}
  \begin{itemize}
    \item Goal: finding build dependencies to add to a build profile
    \item Heuristics are needed because the investigation can only be done
      by a human developer
      \begin{itemize}
        \item Investigation of source code and build system
        \item Decision about best software engineering practices (cross
          building vs. build profile vs. package splitting \ldots)
        \item Decision about trade-off of different kinds of build profiles
        \item Decision about long-term viability
        \item Communication with upstream developers
      \end{itemize}
    \item Different heuristic kinds based on dependency graph syntax (mostly
      ignoring semantics)
      \begin{itemize}
        \item Simple (degree ratios, degree count)
        \item Component based (strong brides and articulation points)
        \item Cycle based
        \item Feedback Arc Set
      \end{itemize}
  \end{itemize}
\end{frame}

%\begin{frame}{HTML Display of Heuristics}
%  \begin{figure}
%    \includegraphics[width=\textwidth]{bootstrapstatsweb}
%    \caption{Table of edges with most cycles through them}
%  \end{figure}
%\end{frame}

\begin{frame}{Simple Heuristics}
  \begin{itemize}
    \item Ratio based heuristics
      \begin{figure}
        \includegraphics[scale=0.4]{{heuristic_1_example.dia}.eps}
      \begin{figure}
      \end{figure}
        \includegraphics[scale=0.4]{{heuristic_2_example.dia}.eps}
      \end{figure}
    \item Amount of missing dependencies
    \item Amount of missing ``weak'' dependencies (build dependencies
      commonly used for documentation generation and unit tests)
  \end{itemize}
\end{frame}

%% Maybe you can add a couple of mini examples to explain what these
%% heuristics work
\begin{frame}{Strong bridges and strong articulation points}
  \begin{figure}
    \includegraphics[height=.8\paperheight]{strongbridges.eps}
  \end{figure}
\end{frame}

\begin{frame}{Small cycles and edges with most cycles through them}
  \begin{figure}
    \includegraphics[scale=0.4]{{buildgraph.dot}.eps}
  \end{figure}
\end{frame}

\subsection{Feedback Arc Set Algorithm}

% add a running example to explain who the algorithm runs in
% practice.
\begin{frame}{Feedback Arc Set Algorithm}
  \begin{enumerate}
    \item Remove all self-cycles and add them to the FAS
    \item Find cycles up to length N
    \item Remove edge with most cycles through them and add it to the
      FAS
    \item If cycles remain, go back to 3, otherwise continue
    \item If graph is still cyclic, increment N and go back to 2
      %% This last sentence is not very clear \dots
      %%
      %% commented out - it's an unnecessary overcomplicating detail
      %% anyway...
    %\item Obtain order induced by Feedback Arc Set, remove forward edges
    %  from the Feedback Arc Set and reintroduce them into the graph
    \item Return obtained FAS
  \end{enumerate}
\end{frame}

\section{Experimental Results}

%% people are not really interested about these kind of details
%% maybe you can say this at the end and provide a pointer to the
%% git repository.
%\begin{frame}{Implementation}
%  \begin{itemize}
%    \item Implemented in OCaml, Python and POSIX Shell
%    \item Using dose3 and ocamlgraph
%    \item Distributed under the terms of the LGPL3+
%  \end{itemize}
%\end{frame}

\begin{frame}{Toolset}
  \begin{itemize}
    \item Freely Available (LGPL3+).
    \item UNIX Philosophy: Multiple application, each executing one
      algorithm connected by pipes
    \item Exchange format is a plain text Debian package description format
    \item Graphs are output in GraphML to be consumed and analyzed by 3rd
      party tools
    \item Existing tools are used where possible
  \end{itemize}
\end{frame}

\begin{frame}{Test Setup}
  \begin{itemize}
    \item Debian Sid, January 2013
      \begin{itemize}
        \item more than 38000 binary packages
        \item more than 18000 source packages
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Benchmark Results}
  \begin{figure}
    \includegraphics[width=\textwidth]{benchmark}
    \caption{Execution time using a self-contained repository (top), normal
      execution (middle) and with computing strong dependencies (bottom)}
  \end{figure}
\end{frame}

\begin{frame}{Quality of Feedback Arc Set Algorithm}
  \begin{itemize}
    \item Information of droppability manually gathered and extracted from
      Gentoo Linux
    \item Biggest strongly connected component with 993 vertices and 9420 edges
  \end{itemize}
  \begin{center}
    \begin{tabular}{c|c|c|c}
      cycle length & modified sources & FAS size & removable \\ \hline
      4 & 53 & 95 & 0.91 \% \\
      6 & 54 & 99 & 0.93 \% \\
      8 & 57 & 96 & 0.91 \% \\
      10 & 57 & 99 & 0.92 \% \\
      12 & 53 & 93 & 0.91 \% \\
    \end{tabular}
  \end{center}
\end{frame}

\section{Summary}

\begin{frame}{Conclusions and Future Work}
  \begin{itemize}
    \item Bootstrapping FOSS distributions used to be a year long manual
      process and it can now be automatic, deterministic and fast
    \item This paper lies the theoretical foundations and the analysis is
      only based on the packages metadata. These tools are going to be used
      this summer to effectively bootstrap the Debian distribution to a new
      hardware platform.
  \end{itemize}
  \begin{itemize}
    \item Improve our heuristics
    \item Add more algorithms
    \item Extend the support of the tool set to other software
      distributions.
    \item Possible use in a different context?
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \Huge{Questions?}
  \end{center}
\end{frame}

%% We don't mention this comparison in the paper, you
%% you might keep it here is somebody ask a pertinent question.
%%
%% Agreed.
\begin{frame}{Comparison with other FAS algorithms}
  \begin{figure}
    \includegraphics[width=\textwidth]{allsols.eps}
    \vspace{4mm}
    \includegraphics[width=\textwidth]{alltimes.eps}
  \end{figure}
\end{frame}


\end{document}

% vim:tabstop=2:shiftwidth=2:expandtab:textwidth=76:
