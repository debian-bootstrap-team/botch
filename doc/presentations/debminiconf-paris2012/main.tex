\documentclass{beamer}
\usetheme[secheader]{Boadilla}
\usepackage{graphicx}
\usepackage[noend]{algpseudocode}
\usepackage{url}
%\usepackage{datatool}

\title[Bootstrapping Debian]{Bootstrapping Debian for a new
architecture \\ A talk about challenges !}
\author[P. Abate]{Pietro Abate}
\institute[P7/Irill/Inria]{Universite Paris Diderot}
\date{24-11-2012}

\begin{document}

\frame{\titlepage}

\section{Introduction}

\begin{frame}
  \frametitle{Acknowledgements}
  \begin{itemize}
    \item Most of the work done by Johannes Schauer during GSOC 2012
    \item Mentoring myself and Wookie
  \end{itemize}
\end{frame}

\frame{
  \frametitle{Problem}
  \begin{itemize}
    \item Debian was ported to more than 20 architectures so the 
      process is executed roughly once per year
    \item Debian packages are neither made to be cross compilable nor
      to be built without an existing full Debian installation
    \item For each new port a \texttt{set} of source packages has to 
      be cross compiled and/or built manually
    \item Bootstrap a new architecture often involves foreign 
      distributions and a lot of hacking
  \end{itemize}
}

\frame{ 
  \frametitle{Wish List}
  \begin{itemize}
    \item Porting Debian to a new architecture should be less time 
      consuming and less problematic.
    \item No foreign distributions during porting (self hosted).
    \item Automatic cross compiling for architectures that cannot build 
      themselves.
    \item Sub-arch builds optimized for a specific CPU should be
      easier.
  \end{itemize}
}

\frame{         
  \frametitle{The final Goal : Deducing a build order}
  \begin{enumerate}
    \item Step zero : Bare metal.
    \item Cross compilation : create a minimal build system ($XC$).
    \item Automatic (cross) compile $XC$ (we need a build order and
      correct Multi-Arch annotations).
    \item Switch to native compilation.
    \item Find the largest number of sources that can be natively
      built ($NC$).
    \item Automatic compile $NC$ (we need a build order).
  \end{enumerate}
  \textbf{The Real Problem} : build dependencies cycles.
}

\frame{
  \frametitle{Cross Compilation Vs. Native Compilation}
  \begin{itemize}
    \item A new architecture cannot be bootstrapped from thin air
    \item At least a minimal system must be cross built
    \item This system should be large enough to compile the entire
      distribution
    \item Native compilation should be preferred over cross compilation
  \end{itemize}
}

\section{Bootstrap for a new architecture}

\frame{
  \frametitle{Cross compilation. Package selection.}

    The minimal set of packages that must be cross compiled ($XC$) are 
    those with the following properties :

    \begin{itemize}
      \item \texttt{Essential: yes}
      \item \texttt{Build-Essential: yes}
      \item \texttt{Priority: required}
%      \item \texttt{Package: build-essential}
    \end{itemize}

    Plus debhelper as 79\% of the archive depend on it
}

\frame{
  \frametitle{Minimal build system}
  \begin{itemize}
    \item How many packages are in the minimal build system for Sid ?

      \begin{tabular}{lll}         
                                      & Debian Sid & Ubuntu Precise \\ \hline
        \texttt{Priority: required}   & 37         & 70 \\
        \texttt{Essential: yes}       & 25         & 24 \\
        \texttt{Build-Essential: yes} & 11         & 44 \\
        the above plus dependencies   & 106        & 140 \\
        number of source packages     & 55         & 75 \\ \hline
      \end{tabular}
    
    \item Many packages in $XC$ would cross-build just fine if their
    cross-build-dependencies could be resolved using Multi-Arch.
    \item \texttt{Challenge N. 1} : Automatically Cross compile the minimal build
    system.
  \end{itemize}
}

\begin{frame}[fragile]
\small
  \frametitle{Test cross-build-dependency resolution}
  With \texttt{apt-get} (adding an armel as foreign architecture):
  \begin{verbatim}
apt-get --simulate --host-architecture=armel build-dep <package>
  \end{verbatim}

  Or with \texttt{dose-buildebcheck} (static check):
  \begin{verbatim}
dose-builddebcheck -s --deb-native-arch=amd64 --deb-host-arch=armhf \
 ubuntu_dists_quantal_main_binary-amd64_Packages \
 ubuntu_dists_quantal_main_source_Sources
  \end{verbatim}
\end{frame}

\frame{
  \frametitle{We can't cross compile the minimal build system (yet !)}
  \includegraphics[width=\linewidth]{xcompile.png}
}


\frame{
  \frametitle{Which packages can be natively compiled ?}
  Maximal set of source package that can be compiled natively from $XC$.
  \begin{figure}[t]
    \caption{Compilation Fix Point}
    \label{alg:source-fixpoint}
    \begin{algorithmic}[1]
      \Procedure{F}{$C_i, B_i, S_i$}
        \State $\mathtt{NS} \gets find\_installable (B_i,S_i)$
        \Comment{Sources compilable in $B_i$}
        \If {$\mathtt{NS} = \emptyset$}
          \State \textbf{return} {$(B_i,C_i)$}
        \Else
         \State $\mathtt{B_{i+1}} \leftarrow Bin(NS) \cup B_i$
         \Comment{$B_i$ plus all binaries from $NS$}
         \State $\mathtt{C_{i+1}} \leftarrow C_i \cup NS$
         \Comment{Overall set of compilable sources}
         \State $\mathtt{S_{i+1}} \leftarrow S_i \setminus NS$
         \Comment{Sources left to compile}
         \State \textbf{return} {$F (C_{i+1}, B_{i+1}, S_{i+1})$}
        \EndIf
      \EndProcedure
    \State $Fixpoint \gets F (\emptyset,Bin(XC),S)$
    \end{algorithmic}
  \end{figure}
}

\frame{
  \frametitle{Which packages are needed to build all the sources in $S$}
  We assume all source packages can be compiled in $U$
  \begin{figure}[t]
    \label{alg:source-closure}
    \begin{algorithmic}[1]
      \Procedure{F}{$B_i, C_i, U, S$}
        \State $\mathtt{NB} \gets compute\_is (U,S)$
        \Comment{$NB = \bigcup_{s \in S} IS(s)$}
        \If {$\mathtt{NB} = \emptyset$}
          \State \textbf{return} {$(B_i,C_i)$}
        \Else
         \State $\mathtt{B_{i+1}} \leftarrow B_i \cup NB$
         \State $\mathtt{C_{i+1}} \leftarrow C_i \cup S$
         \State $\mathtt{NS} \leftarrow Src(NB) \setminus C_{i+1}$
         \State \textbf{return} {$F (B_{i+1}, C_{i+1}, U, NS)$}
        \EndIf
      \EndProcedure
    \State $Closure \gets F (\emptyset,\emptyset,U,S)$
    \end{algorithmic}
    \caption{Build Closure}
  \end{figure}
}

%
%\frame{
%  \frametitle{Questions}
%  \begin{itemize}
%    \item How many packages can be natively compiled from $XC$ ?
%    \item How many source packages ?
%    \item What if we don't include debhelper in $XC$ ?
%    \item Why we can't compiled the entire archive ?
%  \end{itemize}
%}

\section{Results}

\frame{
  \frametitle{The dependency graph (1/2)}
  \includegraphics[width=\linewidth]{buildgraph.png}
  \begin{itemize}
    \item Two types of vertex
      \begin{itemize}
        \item source packages.
        \item build-dependency set (binaries needed to build a source
          package)
      \end{itemize}
    \item Two types of edges
      \begin{itemize}
        \item build-dep (source $\rightarrow$ binary)
        \item built-from (binary $\rightarrow$ source)
      \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{The dependency graph (2/2)}

  \begin{itemize}
    \item Built iteratively by adding connecting each source package
      to the set of its build dependencies and each build dependencies
      set to all source packages whose binaries are build from.
    \item Packages that are cross-built ($p \in XC$) or with
      \texttt{Architecture:all} are excluded from the dependency
      graph.
  \end{itemize}
}

\begin{frame}
  \frametitle{Simplify the Build Dependency Graph. Challenge N. 2}
  \begin{itemize}
    \item The control fields \texttt{Build-Depends-Indep} and
      \texttt{Build-Conflicts-Indep} identify dependencies or
      conflicts for building architecture:all packages
    \item We are not interested to build architecture:all packages
      therefore we can drop \texttt{Build-Depends-Indep} and
      \texttt{Build-Conflicts-Indep} dependencies
    \item Find Weak dependencies :
      \begin{itemize}
        \item Manually identify packages that are not strictly needed
          to compile a working, albeit not full, package
        \item Use external information to identify weak packages
          (gentoo compile flags)
        \item Add \textbf{build profiles} (ex. stage1, embedded,
          nodoc, etc) to source packages (more later about build
          profiles).
      \end{itemize} 
  \end{itemize}
\end{frame} 

\frame{
  \frametitle{Some numbers on the build graph}
    \begin{itemize}
      \item the dependency graph generated for Debian Sid has 
        39486 vertices.
      \item it has only one central SCC with 1027 vertices.
      \item eight other SCC with 2 to 7 vertices.
      \item contains not-nice packages like: nautilus, iceweasel, 
        metacity, evolution, etc
      \item contains many build dependency cycles.
      \item \texttt{Challenge N. 3} (Automatically) Remove build dependencies
    \end{itemize}
}

\frame{
  \frametitle{Dependency graph analysis}
  We can easily identify :
  \begin{itemize}
    \item binary/source nodes with most/least incoming/outgoing edges
    \item most/least connected nodes
%    \item highest/lowest ratios
    \item source packages only missing a few build dependencies
    \item binary packages with highest ratio of source packages it 
        needs to be built and source packages that build depend on it
        \includegraphics[width=\linewidth]{reduced_deps_1.png}
    \item source packages with highest ratio of build dependencies and 
        source packages that build-depend on packages that depend on it
        \includegraphics[width=\linewidth]{reduced_deps_2.png}
  \end{itemize}
}               

\frame{                 
  \frametitle{Current Unresolved Issue in Debian}
  \begin{itemize}
    \item Provide a build order is still difficult because :
      \begin{itemize}
        \item unsatisfied cross build dependencies because of missing 
          multi-arch annotation
        \item insufficient number of reduced build dependencies to solve 
          dependency cycles
      \end{itemize}
    \item what is blocking the above:
      \begin{itemize} 
        \item wanna-build doesn't support architecture qualifiers 
          (\texttt{pkg:any}, \texttt{pkg:native}, \texttt{pkg:amd64}, ...)
        \item no decision on format of reduced build dependencies
      \end{itemize}
    \item after both issues are solved, changes have to be manually 
      implemented into actual packages
  \end{itemize}
}

\frame{         
  \frametitle{Adding build profiles}
  \begin{itemize}
    \item A build profile is a global build dependency filter
    \item It is the form : \texttt{Build-Depends: foo [i386 arm] <!stage1>}
    \item The format similar to architecture specifiers
    \item Trivial to implement
    \item Avoid duplication of build dependency list
  \end{itemize}
}

\section{Conclusions}

\begin{frame}
  \frametitle{Future work}
  \begin{itemize}
    \item Identify a list of plausible weak dependencies (Work in
      progress to use Gentoo build-flags)
    \item Devise an algorithm to automatically break build cycles
      using weak dependencies (almost done)
    \item Create a topological sort of the build dependency graph
      (almost done)
    \item Provide a build order to be used to bootstrap debian of a
      foreign architecture.
    \item Generalize this solution to a larger class of problems.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Tools and Resources}
  All our tools and experiments are available :
  \begin{itemize}
    \item Debian Bootstrap :
      \url{https://gitorious.org/debian-bootstrap/bootstrap}
    \item Dose : \url{https://gforge.inria.fr/projects/dose/}
    \item dose-builddebcheck :
      \url{http://packages.debian.org/wheezy/dose-builddebcheck}
  \end{itemize}

  \begin{itemize}
    \item Main page : \url{http://wiki.debian.org/DebianBootstrap}
    \item Lots of details :
      \url{http://wiki.debian.org/DebianBootstrap/TODO}
    \item Multi-Arch Cross spec
      \url{https://wiki.ubuntu.com/MultiarchCross}
    \item Multi-Arch spec : \url{https://wiki.ubuntu.com/MultiarchSpec}
    \item Linaro Cross Compile Howto
      \url{https://wiki.linaro.org/Platform/DevPlatform/CrossCompile/UsingMultiArch}
  \end{itemize}
\end{frame}

\end{document}
