#/usr/bin/python

import pydot

graph = pydot.Dot(graph_type='digraph',rankdir="LR")

src1 = pydot.Node("Src 1", style="filled", fillcolor="red")
src2 = pydot.Node("Src 2", style="filled", fillcolor="red")

bin1 = pydot.Node("Bin 1", style="filled", fillcolor="green")
bin2= pydot.Node("Bin 2", style="filled", fillcolor="green")

graph.add_node(src1)
graph.add_node(src2)

graph.add_node(bin1)
graph.add_node(bin2)

graph.add_edge(pydot.Edge(src1, bin1,label="buildep"))
graph.add_edge(pydot.Edge(bin1, src2,label="builtfrom"))
graph.add_edge(pydot.Edge(src2, bin2,label="buildep"))
graph.add_edge(pydot.Edge(bin2, src1,label="builtfrom"))

graph.write_png('buildgraph.png')
