\documentclass{beamer}
\usetheme[secheader]{Boadilla}

\title[An introduction to the Bootstrap/Build Ordering Toolchain]{Solving the bootstrap problem for Debian based operating systems}
\author[J. Schauer]{Johannes 'josch' Schauer}
\institute[JUB]{Jacobs University Bremen}
\date{DebConf 2013, Vaumarcus}

\begin{document}

\frame{\titlepage}

\section{Introduction}

\frame { \frametitle{Overview}
        \begin{itemize}
		\item botch = Bootstrap/Build Ordering ToolCHain
                \item Started as Debian Google Summer of Code project 2012
                \item Continued as my master thesis at Jacobs University Bremen 
                \item Mentors:
                        \begin{itemize}
                                \item \textbf{Wookey} practical side
                                \item \textbf{Pietro Abate} theoretical/academic side
                        \end{itemize}
        \end{itemize}
}

\subsection{Problem Description}

\frame { \frametitle{Common Case}
        \begin{itemize}
                \item Source packages are always natively compiled
                \item Source packages are compiled with the full archive of binary packages available
        \end{itemize}
}

\frame { \frametitle{During Bootstrapping}
        \begin{itemize}
                \item Some source packages must be cross compiled
                \item Only a few binary packages are available $\rightarrow$ dependency cycles
        \end{itemize}
}

\frame { \frametitle{Dependency Graph in Debian Sid}
        \includegraphics[width=\textwidth]{hideous_mess.png}
}

\frame { \frametitle{Development of problem size}
        \includegraphics[width=\textwidth]{history_scc.pdf}
}

\frame { \frametitle{Current Bootstrapping Practice}
        \begin{itemize}
                \item Using Gentoo or OpenEmbedded to avoid cross compilation of the base system
                \item Manual dependency cycle analysis
                \item Manual hacking of source packages to build with fewer build dependencies
                \item Takes several months up to a year to complete
        \end{itemize}
}

\subsection{Benefits to Debian}

\frame { \frametitle{What if bootstrapping was easier?}
        \begin{itemize}
                \item Easier porting for upcoming architectures
                \item More custom ports, optimized for a specific CPU
                \item Remove the need of Gentoo or OpenEmbedded (make Debian more universal)
        \end{itemize}
}

\frame { \frametitle{Wait, there is more!}
        \begin{itemize}
                \item Update lagging architectures
                \item Build for targets that can't build themselves (once cross building gained better support)
                \item QA tool which allows to check the archive for bootstrappability
                \item Order rebuilds for library transitions (Haskell, OCaml)
        \end{itemize}
}

\subsection{What this talk is about}

\frame { \frametitle{The essence of this talk}
        \begin{itemize}
                \item The core algorithms for graph analysis exist and they are fast and seem to be correct
                \item We need decisions about new dependency syntaxes, multiarch and cross building to do the practical plumbing
        \end{itemize}
}

\frame { \frametitle{The tools}
        \begin{itemize}
                \item Written in OCaml, Python, Shell
		\item LGPL3+
                \item Using dose3 as helper library (parser, solver, ...)
		\item UNIX Philosophy: Multiple application, each executing one algorithm connected by pipes
		\item Exchange format is a plain text Debian package description format
		\item Graphs are output in GraphML to be consumed and analyzed by 3rd party tools
                \item Git: \url{https://gitorious.org/debian-bootstrap/botch}
        \end{itemize}
}

\frame { \frametitle{More specifically we can now...}
        \begin{itemize}
                \item ... create \& analyze a dependency graph
                \item ... find source packages to modify
                \item ... create a build order
        \end{itemize}
}

\frame { \frametitle{It's only theory}
        \begin{itemize}
                \item Tools only work on package meta data
                \item No source packages are compiled, no binary packages installed
        \end{itemize}
}

\frame { \frametitle{What is needed to test in practice}
        \begin{itemize}
                \item Reduced build dependencies (build profiles)
		\item Cross compilation support in base packages
        \end{itemize}
}

\begin{frame}{Bootstrap Workflow}
  \begin{enumerate}
    \item Select binary packages for minimal build system and cross compile
      them for the new platform
    \item Create Build Graph and extract strongly connected components
    \item If the amount of existing build profiles is not enough to make
      build graph acyclic:
      \begin{itemize}
        \item use heuristics to find source packages to add build profiles to
        \item modify the respective source packages
        \item go back to 2
      \end{itemize}
    \item Select source packages to build with a build profile, making the graph acyclic
    \item Deduce a build order
  \end{enumerate}
\end{frame}

\frame { \frametitle{Breaking dependency cycles}
        \begin{itemize}
                \item Remove build dependencies (build profiles)
                \item Move dependencies from \texttt{Build-Depends} to \texttt{Build-Depends-Indep}
                \item Choose different installation sets for not-strong dependencies
                \item Make binary packages available through cross compilation
		\item Use existing \texttt{Multi-Arch:foreign} packages
		\item Split a source package
        \end{itemize}
}

\begin{frame}{Heuristics}
  \begin{itemize}
    \item Goal: finding source packages to modify
    \item Heuristics are needed because the investigation can only be done
      by a human developer
    \item Different heuristic kinds based on dependency graph syntax (mostly
      ignoring semantics)
      \begin{itemize}
        \item Simple (degree ratios, degree count)
        \item Component based (strong brides and articulation points)
        \item Cycle based
        \item Feedback Arc Set
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{HTML Display of Heuristics}
	\begin{itemize}
		\item \url{http://mister-muffin.de/bootstrap/stats/}
	\end{itemize}
  \begin{figure}
    \includegraphics[width=\textwidth]{bootstrapstatsweb}
    \caption{Table of edges with most cycles through them}
  \end{figure}
\end{frame}

\begin{frame}{Simple Heuristics}
  \begin{itemize}
    \item Ratio based heuristics
      \begin{figure}
        \includegraphics[scale=0.3]{{heuristic_1_example.dia}.eps}
      \begin{figure}
      \end{figure}
        \includegraphics[scale=0.3]{{heuristic_2_example.dia}.eps}
      \end{figure}
    \item Amount of missing dependencies
    \item Amount of missing ``weak'' dependencies (build dependencies
      commonly used for documentation generation and unit tests)
  \end{itemize}
\end{frame}

\begin{frame}{Strong bridges}
  \begin{figure}
    \includegraphics[height=.8\paperheight]{strongbridges.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Strong bridges}
  \begin{figure}
    \includegraphics[width=\textwidth]{strongbridges_solved.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Strong articulation points}
  \begin{figure}
    \includegraphics[height=.8\paperheight]{strongarticulationpoints.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Strong articulation points}
  \begin{figure}
    \includegraphics[width=\textwidth]{strongarticulationpoints_solved.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Small cycles}
  \begin{figure}
	  \includegraphics[height=.8\paperheight]{scc_50702_src:libusbx_6.pdf}
  \end{figure}
\end{frame}

\begin{frame}{HTML Display of Self-Cycles}
	\begin{itemize}
		\item \url{http://mister-muffin.de/bootstrap/selfcycles.html}
	\end{itemize}
  \begin{figure}
    \includegraphics[height=.7\paperheight]{selfcyclesstatsweb}
    \caption{Table of indirect self-cycles}
  \end{figure}
\end{frame}

\begin{frame}{Edges with most cycles through them}
	\begin{itemize}
		\item SCC with 15 vertices, 31 edges
	\end{itemize}
  \begin{figure}
	  \includegraphics[width=\textwidth]{scc_48816_src:libx11_15.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Edges with most cycles through them}
	\begin{itemize}
		\item SCC with 15 vertices, 31 edges
	\end{itemize}
  \begin{figure}
	  \includegraphics[width=\textwidth]{scc_48816_src:libx11_15_solved.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Calculating a Feedback Arc Set}
	\begin{itemize}
		\item Debian Sid as of 2013-0816T095432Z
		\item 28272 vertices, 249686 edges
		\item biggest SCC: 1080 vertices, 11726 edges
		\item assuming everything can be broken
			\begin{itemize}
				\item breakable by modifying 51 source packages
			\end{itemize}
		\item more realistic using Gentoo and lists by Thorsten Glaser, Patrick McDermott, Daniel Schepler, Wookey
			\begin{itemize}
				\item breakable by modifying 57 source packages
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Benchmark Results}
  \begin{figure}
    \includegraphics[width=\textwidth]{benchmark}
    \caption{Execution time using a self-contained repository (top), normal
      execution (middle) and with computing strong dependencies (bottom)}
  \end{figure}
\end{frame}

\frame { \frametitle{Resources}
        \begin{itemize}
		\item Slides: \url{http://mister-muffin.de/bootstrap/debconf13.pdf}
                \item Blog: \url{http://blog.mister-muffin.de}
                \item ML: \texttt{debian-bootstrap [at] lists.mister-muffin.de} 
                \item IRC: \texttt{\#debian-bootstrap [at] irc.oftc.net}
                \item Git1: \url{https://gitorious.org/debian-bootstrap/botch}
                \item Git2: \url{https://gitorious.org/debian-bootstrap/gen2deb}
                \item Git3: \url{https://github.com/josch/cycle_test}
                \item Dose3: \url{https://gforge.inria.fr/projects/dose/}
                \item Wiki1: \url{http://wiki.debian.org/DebianBootstrap}
                \item Wiki2: \url{http://wiki.debian.org/DebianBootstrap/TODO}
		\item Wiki3: \url{https://gitorious.org/debian-bootstrap/pages/Home}
		\item Thesis: \url{http://mister-muffin.de/bootstrap/thesis.pdf}
                \item Profiles: \url{https://l.d.o/debian-devel/2013/01/msg00329.html}
        \end{itemize}
}

\frame { \frametitle{Conclusion}
	\begin{itemize}
		\item We could have:
			\begin{itemize}
				\item Easier porting
				\item More custom ports
				\item Remove the need of Gentoo or OpenEmbedded
				\item Update lagging architectures
				\item Build for targets that can't build themselves
				\item QA tool checking for bootstrappability
			\end{itemize}
		\item But we are missing:
			\begin{itemize}
				\item Decision on the build profile format
				\item Several fixes to support cross compilation
			\end{itemize}
	\end{itemize}
}

\frame { \frametitle{Questions}
        \begin{center}
		\huge{Questions?}
        \end{center}
}

\end{document}
