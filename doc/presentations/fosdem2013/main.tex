\documentclass{beamer}
\usetheme[secheader]{Boadilla}

\title[Bootstrapping Debian]{Bootstrapping Debian-based distributions for new architectures}
\author[J. Schauer]{Johannes 'josch' Schauer}
\institute[JUB]{Jacobs University Bremen}
\date{FOSDEM 2013, Brussels}

\begin{document}

\frame{\titlepage}

\section{Introduction}

\frame { \frametitle{Overview}

	\begin{itemize}
		\item Started as Debian Google Summer of Code project 2012
		\item Continued as my master thesis at Jacobs University Bremen
		\item Mentors:
			\begin{itemize}
				\item \textbf{Wookey} practical side of things
				\item \textbf{Pietro Abate} theoretical and academic side of things
			\end{itemize}
	\end{itemize}
}

\frame { \frametitle{Assumptions}
	\begin{itemize}
		\item Source packages are always natively compiled
		\item Source packages are compiled with the full archive of binary packages available
	\end{itemize}
}

\frame { \frametitle{During Bootstrapping}
	\begin{itemize}
		\item Some source packages must be cross compiled
		\item Only a few binary packages are available $\rightarrow$ dependency cycles
	\end{itemize}
}

\frame { \frametitle{Dependency Graph in Debian Sid}
	\includegraphics[width=\textwidth]{hideous_mess.png}
}

\frame { \frametitle{Development of problem size}
	\includegraphics[width=\textwidth]{history_scc.png}
}

\frame { \frametitle{Current Bootstrapping Practice}
	\begin{itemize}
		\item Using Gentoo or OpenEmbedded to avoid cross compilation
		\item Manual dependency cycle analysis
		\item Manual hacking of source packages to build with fewer build dependencies
		\item Takes up to a year to complete
	\end{itemize}
}

\frame { \frametitle{How needed bootstrapping is}
	\begin{itemize}
		\item 20 Debian ports so about 1 port per year
	\end{itemize}
}

\frame { \frametitle{What if bootstrapping was easier}
	\begin{itemize}
		\item Porting for upcoming architectures easier and faster
		\item More subarch builds, optimized for a specific CPU
	\end{itemize}
}

\frame { \frametitle{What else would be nice}
	\begin{itemize}
		\item Remove the need of Gentoo or OpenEmbedded
		\item Update lagging architectures
		\item QA tool which allows to check the archive for bootstrappability
	\end{itemize}
}

\frame { \frametitle{The essence of this talk}
	\begin{itemize}
		\item We now have the algorithms to automatically do all this
	\end{itemize}
}

\frame { \frametitle{The tools}
	\begin{itemize}
		\item Written in OCaml, Python, Shell
		\item Using dose3 as helper library
		\item Git: \url{https://gitorious.org/debian-bootstrap/bootstrap}
		\item Code contributions by Pietro Abate
	\end{itemize}
}

\frame { \frametitle{More specifically we can now...}
	\begin{itemize}
		\item ... create \& analyze a dependency graph
		\item ... find source packages to modify
		\item ... create a build order
	\end{itemize}
}

\frame { \frametitle{It's only theory}
	\begin{itemize}
		\item Tools only work on package meta data
		\item No source packages are compiled, no binary packages installed
		\item Ignoring the practical implementation of cross compilation, reduced build dependencies
	\end{itemize}
}

\frame { \frametitle{What is needed to test in practice}
	\begin{itemize}
		\item More multiarch (cross compilation)
		\item Better cross compilation support in base packages
		\item Reduced build dependencies (build profiles)
	\end{itemize}
}

\section{Cross Phase}

\frame { \frametitle{Cross compilation}
	\begin{enumerate}
		\item Select packages for minimal native build system
			\begin{itemize}
				\item \texttt{essential:yes}
				\item \texttt{build-essential}
				\item \texttt{debhelper}
			\end{itemize}
		\item Get their co-installation set
		\item Get their source packages
	\end{enumerate}
}

\frame { \frametitle{Algorithm: select packages to cross compile}
	\begin{enumerate}
		\item Add source packages to result
		\item Get foreign cross build dependencies
		\item Get source packages to build them
		\item If not in result go to (1)
	\end{enumerate}
}

\frame { \frametitle{\texttt{Multi-Arch} conflicts}
	\begin{itemize}
		\item Cannot resolve cross build dependencies of some source packages due to \texttt{Multi-Arch} conflicts
		\item Ignoring cross for now
		\item Assumption: minimal native build system can be created from nothing
		\item Later: building and solving dependency graph just as in native phase
	\end{itemize}
}

\section{Native Phase}

\frame { \frametitle{Native compilation}
	\begin{itemize}
		\item Starting from minimal native build system, create and analyze a dependency graph
		\item Debian is huge (18k source, 38k binary)
		\item Create reduced distribution first
	\end{itemize}
}

\frame { \frametitle{Reduced distribution}
	\begin{itemize}
		\item A selection of source packages and binary packages:
			\begin{itemize}
				\item All binary packages must be created by the source packages
				\item All source packages are compilable
			\end{itemize}
	\end{itemize}
}

\frame { \frametitle{Development of reduced distribution size}
	\includegraphics[width=\textwidth]{history_reduced.png}
}

\frame { \frametitle{Selecting packages for a reduced distribution}
	\begin{enumerate}
		\item For example:
			\begin{itemize}
				\item \texttt{essential:yes}
				\item \texttt{build-essential}
				\item \texttt{debhelper}
			\end{itemize}
		\item Get their co-installation set
		\item Get their source packages
	\end{enumerate}
}

\frame { \frametitle{Algorithm: select packages for reduced distribution}
	\begin{enumerate}
		\item Add source packages to result
		\item Get build dependencies
		\item Get source packages to build them
		\item If not in result go to (1)
	\end{enumerate}
}

\section{Build Graph}

\frame { \frametitle{Build graph}
	\begin{itemize}
		\item A dependency graph
		\item Vertices
			\begin{itemize}
				\item Source packages
				\item Installation sets
			\end{itemize}
		\item Edges
			\begin{itemize}
				\item Build-depends (source $\rightarrow$ installation sets)
				\item Builds-from (installation set $\rightarrow$ sources)
			\end{itemize}
	\end{itemize}
}

\frame { \frametitle{Building the graph}
	\begin{itemize}
		\item Connect source packages to installation sets of their build dependencies (except installable ones)
		\item Connect installation sets to source packages they build from (except available ones)
	\end{itemize}
}

\frame { \frametitle{Example build graph}
	\includegraphics[width=\textwidth]{build_graph.eps}
}

\section{Graph analysis}

\frame { \frametitle{Breaking dependency cycles}
	\begin{itemize}
		\item Remove build dependencies (build profiles)
		\item Move dependencies from \texttt{Build-Depends} to \texttt{Build-Depends-Indep}
		\item Choose different installation sets for not-strong dependencies
		\item Make binary packages available through cross compilation
	\end{itemize}
}

\frame { \frametitle{Finding source packages to modify}
	\begin{itemize}
		\item Least amount of build dependencies missing
		\item Ratios
	\end{itemize}
	\center\includegraphics[width=0.5\textwidth]{reduced_deps_1.png}
	\center\includegraphics[width=0.7\textwidth]{reduced_deps_2.png}
	\begin{itemize}
		\item List small cycles
		\item Edges part of most cycles
		\item Calculate Feedback Arc Set
	\end{itemize}
}

\frame { \frametitle{Creating a build order}
	\begin{itemize}
		\item Feedback vertex set problem: find a small amount of source packages to profile build and make build graph acyclic
		\item Convert build graph into source graph
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.3\textwidth]{build_graph.eps}
		\quad
		\includegraphics[width=0.3\textwidth]{build_graph_acyclic.eps}
		\quad
		\includegraphics[width=0.3\textwidth]{build_graph_acyclic_src.eps}
	\end{center}
	\begin{itemize}
		\item Topologically sort source vertices
			\begin{enumerate}
				\item \texttt{src:python2.7(*)}
				\item \texttt{src:x11proto-core}, \texttt{src:libxau}, \texttt{src:tk8.5}, \texttt{src:libxcb(*)}
			\end{enumerate}
	\end{itemize}
}

\frame { \frametitle{Demo time!}
	\begin{itemize}
		\item Debian Sid 1. January 2013
		\item reduced distribution: 613 source packages, 2044 binary packages in 75~seconds
		\item full distribution: 18613 source packages, 38433 binary packages in 9~minutes
		\item credit for reduced build dependencies goes to Gentoo, Thorsten Glaser, Patrick McDermott, Daniel Schepler, Wookey
	\end{itemize}
}

\frame { \frametitle{TODO}
	\begin{itemize}
		\item Try it out in real life
			\begin{itemize}
				\item More multiarch
				\item More cross compilation
				\item Decide for a build profile syntax \& field names
				\item Implement build profiles
			\end{itemize}
		\item Better heuristics
		\item Generalize for larger problem class
		\item Finding a name
	\end{itemize}
}

\frame { \frametitle{Resources}
	\begin{itemize}
		\item Blog: \url{http://blog.mister-muffin.de}
		\item ML: \texttt{debian-bootstrap [at] lists.mister-muffin.de}
		\item IRC: \texttt{\#debian-bootstrap [at] irc.oftc.net}
		\item Git1: \url{https://gitorious.org/debian-bootstrap/bootstrap}
		\item Git2: \url{https://gitorious.org/debian-bootstrap/gen2deb}
		\item Git3: \url{https://github.com/josch/cycle_test}
		\item Dose3: \url{https://gforge.inria.fr/projects/dose/}
		\item Wiki1: \url{http://wiki.debian.org/DebianBootstrap}
		\item Wiki2: \url{http://wiki.debian.org/DebianBootstrap/TODO}
		\item Profiles: \url{https://l.d.o/debian-devel/2013/01/msg00329.html}
	\end{itemize}
}

\frame { \frametitle{Questions}
	\begin{center}
		Questions?
	\end{center}
}

\end{document}
