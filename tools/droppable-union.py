#!/usr/bin/env python3

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_reduced_deps

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Calculate the set union "
            + "of two files of "
            + "droppable build "
            + "dependencies"
        )
    )
    parser.add_argument("droppable1", type=read_reduced_deps, help="file 1")
    parser.add_argument("droppable2", type=read_reduced_deps, help="file 2")
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()

    together = set(args.droppable1.keys()) | set(args.droppable2.keys())

    for srcpkg in sorted(together):
        d1 = args.droppable1.get(srcpkg, set())
        d2 = args.droppable2.get(srcpkg, set())
        print("%s %s" % (srcpkg, " ".join(sorted(d1 | d2))))
