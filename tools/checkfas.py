#!/usr/bin/env python3

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_reduced_deps, read_weak_deps, read_fas


def checkfas(weak, droppable, fas, verbose=False):
    modsrc = 0
    fassize = 0
    notdroppable = 0

    result = list()
    for src, deps in fas:
        modsrc += 1
        for d in deps:
            fassize += 1
            if d not in droppable[src] and d not in weak:
                notdroppable += 1
                result.append((src, d))
    droppable = (fassize - notdroppable) / float(fassize)
    if verbose:
        print(modsrc, fassize, droppable, file=sys.stderr)
    return result


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "given any number of droppable dependencies and a "
            + "feedback arc set, show which edges in the feedback "
            + "arc set are not droppable"
        )
    )
    parser.add_argument(
        "--remove-weak",
        type=read_weak_deps,
        help="A filename containing a list of weak build " + "dependencies",
    )
    parser.add_argument(
        "--remove-reduced",
        type=read_reduced_deps,
        help="One or more filename containing a list of "
        + "droppable build dependencies, separated by "
        + "commas.",
    )
    parser.add_argument("fas", type=read_fas, help="A feedback arc set")
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    result = checkfas(args.remove_weak, args.droppable, args.fas, args.verbose)
    for r in result:
        print(r)
