#!/usr/bin/env python3

import sys
from enum import Enum

sys.path.append("/usr/share/botch")
from util import read_graphml, read_tag_file, human_readable_size


class SearchType(Enum):
    predecessors = 1
    successors = 2
    both = 3


# do a breadth-first-search of a graph and return True if a node that
# satisfies the condition can be found
# do not traverse nodes that satisfy the skip condition
# the direction argument specifies whether to search predecessors,
# successors or both
def bfs(g, n, direction, condition, skip=lambda g, n: False):
    seen = set()
    todo = set([n])
    while todo:
        e = todo.pop()
        if direction == SearchType.predecessors:
            nodes = g.predecessors(e)
        elif direction == SearchType.successors:
            nodes = g.successors(e)
        else:
            raise NotImplementedError(direction)
        for n in nodes:
            if skip(g, n):
                continue
            if condition(g, n):
                return True
            if n not in seen:
                todo.add(n)
        seen.add(e)
    return False


def node_arch_any(g, n):
    return g.nodes[n]["architecture"] != "all"


def node_ma_foreign(g, n):
    return g.nodes[n].get("multiarch", "no") == "foreign"


def get_affected_packages(g):
    result = list()
    for n, d in g.nodes(data=True):
        # M-A: foreign packages are not affected
        if d.get("multiarch", "no") == "foreign":
            continue
        # only Architecture: all packages can be affected
        if d["architecture"] != "all":
            continue
        # source packages are not affected
        if d["type"] == "src":
            continue
        # Walk backward until a predecessor is found which is either an
        # arch:any binary package or a source package that is not arch:all.
        # So essentially we look for all graph nodes that are are not arch:all
        # independent on whether they are binary or source packages.
        # We are only interested in source packages that build more than just
        # arch:all packages because source packages that only build arch:all
        # binary packages do not need to be crossed ever.
        if not bfs(g, n, SearchType.predecessors, node_arch_any):
            continue
        # walk forward until a successor of arch:any is found but
        # stop at m-a:foreign packages
        if not bfs(g, n, SearchType.successors, node_arch_any, node_ma_foreign):
            continue
        # if we end up here, then the package is affected
        pkgname = d["realpackage"]
        if ":" in pkgname:
            pkgname, _ = pkgname.split(":", 1)
        version = d["realversion"]
        result.append((pkgname, version))
    return result


def multiarch_interpreter_problem(g, packages=None, html=False):
    result = dict([(p, 0) for p in get_affected_packages(g)])
    if html:
        print(
            """<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>

    <p>This page lists all Architecture:all and not Multi-Arch:foreign binary
    packages that are on a dependency path between either two Architecture:any
    binary packages, or between a source package and an Architecture:any binary
    package without any Multi-Arch:foreign packages in between.</p>

    <p>More precisely, a graph is created with binary packages and source
    packages being the nodes and their dependency and provides relationship
    being the edges. This page prints all nodes fulfilling all of the following
    criteria:</p>

    <ul>
      <li>The node is a binary package.</li>
      <li>The node is not Multi-Arch:foreign.</li>
      <li>The node is Architecture:all.</li>
      <li>
        The node has a (possibly transitive) predecessor (a reverse dependency)
        which is not Architecture:all or which is a source package.
      </li>
      <li>
        The node has a (possibly transitive) successor (a dependency) that is
        not Architecture:all and not Multi-Arch:foreign. If a
        Multi-Arch:foreign package is encountered while walking the successors,
        then the successors of that package are not further traversed.
      </li>
    </ul>
        """
        )

        print("<p>Number of affected binary packages: %d</p>" % len(result))

    if packages:
        pkg2size = dict()
        for pkg in packages:
            if pkg["Architecture"] != "all":
                continue
            pkg2size[(pkg["Package"], pkg["Version"])] = int(pkg["Size"])
        result = dict([(p, pkg2size[p]) for p in result.keys()])
        if html:
            print(
                "<p>Sum of package sizes: %s</p>"
                % human_readable_size(sum(result.values()))
            )
            print(
                "<p>Sum of 95%% smallest packages: %s</p>"
                % human_readable_size(
                    sum(sorted(result.values())[: (len(result) * 95) // 100])
                )
            )
            print(
                "<p>Sum of 90%% smallest packages: %s</p>"
                % human_readable_size(
                    sum(sorted(result.values())[: (len(result) * 9) // 10])
                )
            )

    if html:
        print("<table>")
        if packages:
            print("<tr><th>Size</th><th>Package name</th></tr>")
        else:
            print("<tr><th>Package name</th></tr>")
        for n, v in sorted(result.keys()):
            print("<tr>")
            if packages:
                print("<td>%d</td>" % result[(n, v)])
            print("<td>%s (= %s)</td></tr>" % (n, v))
        print("<table></body></html>")
    else:
        for n, v in sorted(result.keys()):
            if packages:
                print("%s\t%s\t%s" % (n, v, result[(n, v)]))
            else:
                print("%s\t%s" % (n, v))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "given a dose-ceve pkg graph in graphml format, "
            + "find all multiarch interpreter problems"
        )
    )
    parser.add_argument("g", type=read_graphml, help="Input graph")
    parser.add_argument(
        "-H", "--html", action="store_true", help="output HTML document"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    parser.add_argument(
        "--packages",
        type=read_tag_file,
        help="Packages file to retrieve binary package size ",
    )
    args = parser.parse_args()
    multiarch_interpreter_problem(args.g, args.packages, args.html)
