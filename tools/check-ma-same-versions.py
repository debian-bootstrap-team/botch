#!/usr/bin/env python3

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_tag_file


def check_ma_same_versions(packages1, packages2):
    versions1 = dict()

    for pkg in packages1:
        if pkg.get("Multi-Arch", "").lower() == "same":
            versions1[pkg["Package"]] = pkg["Version"]

    success = True

    missing2 = list()

    for pkg in packages2:
        if pkg.get("Multi-Arch", "").lower() == "same":
            v1 = versions1.get(pkg["Package"])
            if not v1:
                missing2.append(pkg["Package"])
                continue
            if v1 != pkg["Version"]:
                print("%s\t(%s != %s)" % (pkg["Package"], v1, pkg["Version"]))
                success = False
            del versions1[pkg["Package"]]

    if versions1:
        print("only in packages1 and not in packages2:")
        for p in list(versions1.keys()):
            print(p)
        success = False

    if missing2:
        print("only in packages2 and not in packages1:")
        for p in missing2:
            print(p)
        success = False

    return success


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "checks if Multi-Arch: Same packages in two package "
            + "files agree in their versions and also outputs, "
            + "should there be packages in one but not the other"
        )
    )
    parser.add_argument("packages1", type=read_tag_file, help="an input Packages file")
    parser.add_argument(
        "packages2", type=read_tag_file, help="another input Packages file"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    ret = check_ma_same_versions(args.packages1, args.packages2)
    exit(not ret)
