#!/usr/bin/env python3
#
# enough is enough - I do not want to deal with the encoding madness anymore
# and thus this script gets a python3 shebang (descriptions contain non-ascii
# characters)

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_tag_file


def packages_diff(inPackages1, inPackages2, verbose=False):
    pkgs1 = dict()
    for pkg in inPackages1:
        pkgs1[(pkg["Package"], pkg["Architecture"], pkg["Version"])] = pkg

    pkgs2 = dict()
    for pkg in inPackages2:
        pkgs2[(pkg["Package"], pkg["Architecture"], pkg["Version"])] = pkg

    only_in_pkgs1 = set()
    only_in_pkgs2 = set()

    difference = False

    for (pkg1, arch1, ver1) in pkgs1:
        if (pkg1, arch1, ver1) not in pkgs2:
            only_in_pkgs1.add((pkg1, arch1, ver1))
        else:
            pkgstring = "%s:%s (= %s):" % (pkg1, arch1, ver1)
            section1_keys = set(
                [key.lower() for key in list(pkgs1[(pkg1, arch1, ver1)].keys())]
            )
            section2_keys = set(
                [key.lower() for key in list(pkgs2[(pkg1, arch1, ver1)].keys())]
            )
            only_in_section1 = section1_keys - section2_keys
            only_in_section2 = section2_keys - section1_keys
            common_keys = section1_keys & section2_keys
            common_diff_keys = [
                key
                for key in common_keys
                if pkgs1[(pkg1, arch1, ver1)][key] != pkgs2[(pkg1, arch1, ver1)][key]
            ]
            for key in only_in_section1:
                difference = True
                print("%s < %s: %s" % (pkgstring, key, pkgs1[(pkg1, arch1, ver1)][key]))
            for key in only_in_section2:
                difference = True
                print("%s > %s: %s" % (pkgstring, key, pkgs2[(pkg1, arch1, ver1)][key]))
            for key in common_diff_keys:
                difference = True
                print("%s < %s: %s" % (pkgstring, key, pkgs1[(pkg1, arch1, ver1)][key]))
                print("%s > %s: %s" % (pkgstring, key, pkgs2[(pkg1, arch1, ver1)][key]))

    for pkg in pkgs2:
        if pkg not in pkgs1:
            only_in_pkgs2.add(pkg)

    if only_in_pkgs1:
        print("Only in packages1:")
        for (pkg1, arch1, ver1) in only_in_pkgs1:
            print(" %s:%s (= %s)" % (pkg1, arch1, ver1))

    if only_in_pkgs2:
        print("Only in packages2:")
        for (pkg2, arch2, ver2) in only_in_pkgs2:
            print(" %s:%s (= %s)" % (pkg2, arch2, ver2))

    if only_in_pkgs1 or only_in_pkgs2:
        difference = True

    return not (difference)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=("print difference between two Packages files")
    )
    parser.add_argument(
        "inPackages1", type=read_tag_file, help="input Packages file #1"
    )
    parser.add_argument(
        "inPackages2", type=read_tag_file, help="input Packages file #2"
    )
    parser.add_argument("--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    ret = packages_diff(args.inPackages1, args.inPackages2, args.verbose)
    exit(not ret)
