(**************************************************************************)
(*                                                                        *)
(*  Copyright (C) 2012 Johannes 'josch' Schauer <j.schauer@email.de>      *)
(*                                                                        *)
(*  This library is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Lesser General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version.  A special linking    *)
(*  exception to the GNU Lesser General Public License applies to this    *)
(*  library, see the COPYING file for more information.                   *)
(**************************************************************************)

open! ExtLib
open Dose_common
open Dose_debian
open Dose_algo
open Dose_doseparse

#define __label __FILE__
let label =  __label ;;
include Util.Logging(struct let label = label end) ;;

module Options = struct
  open OptParse

  let description = ("Annotate buildgraph.xml and srcgraph.xml with strong dependency information")
  let usage = "%prog buildgraph.xml srcgraph.xml Packages Sources"

  let options = OptParser.make ~description ~usage
  include BootstrapCommon.MakeOptions(struct let options = options end)

  include StdOptions.InputOptions;;
  let default = List.filter (fun e -> not (List.mem e ["compare"; "checkonly"; "latest";"outfile";"inputtype"])) StdOptions.InputOptions.default_options in
  StdOptions.InputOptions.add_options ~default options ;;

  include StdOptions.DistribOptions;;
  let default = List.filter (fun e -> not (List.mem e ["deb-profiles"; "deb-ignore-essential"; "deb-builds-from"])) StdOptions.DistribOptions.default_options in
  StdOptions.DistribOptions.add_debian_options ~default options ;;
end

let main () =
  let posargs = OptParse.OptParser.parse_argv Options.options in
  StdDebug.enable_debug (OptParse.Opt.get Options.verbose);
  Util.Debug.disable "Depsolver_int";
  StdDebug.all_quiet (OptParse.Opt.get Options.quiet);

  let options = Options.set_deb_options () in
  let noindep = options.Debcudf.drop_bd_indep in
  let buildarch = Option.get options.Debcudf.native in
  let hostarch = match options.Debcudf.host with None -> "" | Some s -> s in
  let foreignarchs = options.Debcudf.foreign in

  let bgf, sgf, posargs = match posargs with
   | bgf::sgf::l -> bgf,sgf,l
   | _ -> fatal "you must provide buildgraph, srcgraph, Packages and Sources"
  in

  let (binlist, (fgsrclist,bgsrclist), _) = BootstrapCommon.parse_packages ~noindep Options.parse_cmdline buildarch hostarch foreignarchs posargs in

  let tables = Debcudf.init_tables (fgsrclist@bgsrclist@binlist) in
  let fgsl = List.map (Debcudf.tocudf ?inst:None ~options tables) fgsrclist in
  let bgsl = List.map (Debcudf.tocudf ?inst:None ~options tables) bgsrclist in
  let pkglist = List.map (Debcudf.tocudf ?inst:None ~options tables) binlist in
  let universe = Cudf.load_universe (BootstrapCommon.unique [pkglist;fgsl;bgsl]) in

  let ic = open_in bgf in
  let bg = BuildGraph.from_ic universe buildarch ic in
  close_in ic;

  let ic = open_in sgf in
  let sg = SrcGraph.from_ic universe buildarch ic in
  close_in ic;

  let module BE = BuildGraphExtras.Make(struct let univ = universe end) in
  let module SE = SrcGraphExtras.Make(struct let univ = universe end) in

  let sdg = Strongdeps.strongdeps_univ universe in
  let is_strong pkg dep = (CudfAdd.compare pkg dep = 0) || (BootstrapCommon.pkg_list_mem (Defaultgraphs.PackageGraph.succ_list sdg pkg) dep) in
  let bg = BE.annotate is_strong bg in
  let sg = SE.annotate is_strong sg in

  let module BGP = BuildGraph.Printer(struct let univ = universe end) in
  let module SGP = SrcGraph.Printer(struct let univ = universe end) in

  let oc = open_out bgf in
  BGP.print (Format.formatter_of_out_channel oc) bg;
  close_out oc;
  
  let oc = open_out sgf in
  SGP.print (Format.formatter_of_out_channel oc) sg;
  close_out oc;
;;

main ();;
