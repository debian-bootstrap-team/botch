IMPORTANT
---------

All the below is outdated and has to be rewritten. For more up-to-date instructions go to https://salsa.debian.org/debian-bootstrap-team/botch/-/wikis/home and read the articles linked from there.

First steps
-----------


Because there was no dose3 upstream release since January 2013, botch needs
some features of dose3 which are only available via git. Dose3 itself can only
release a new version once a new upstream version of libcudf is released. So
dose3 depends on the git version of libcudf as well.

To check out the dose submodule you need git. To check out the libcudf
submodule you need ca-certificates.

$ apt-get install git ca-certificates
$ git submodule update --init
$ (cd dose; git submodule update --init;)

You now can install all dependencies to build libcudf, dose3 and botch:

$ apt-get install ocaml-nox camlp4 libbz2-ocaml-dev ocaml-findlib libextlib-ocaml-dev libre-ocaml-dev libocamlgraph-ocaml-dev
$ make doselib
$ apt-get install libatdgen-ocaml-dev libxmlm-ocaml-dev
$ make
$ apt-get install dctrl-tools wget python python-apt python-networkx
$ ./tools/native.sh

The result is...

You can also use graphviz:

$ apt-get install python-pygraphviz
$ ./tools/graphml2dot


Install dependencies

    $ apt-get install --no-install-recommends libdose3-ocaml-dev camlp4 make \
    > wget python-apt bzip2 patch libatdgen-ocaml-dev
    $ apt-get install {any www-browser of your choice}

 - libdose3-ocaml-dev, camlp4, make and python-apt are essential
 - wget is only needed for `make setup` execution
 - bzip2 and patch are only needed for `make setup-sid`
 - www-browser is needed to display the statistics output

If you want to use upstream master dose version

    $ git submodule update --init
    $ make doselib

Compile project

    $ make

Retrieve sample Debian Sid Sources.bz2 and Packages.bz2

    $ make setup-arm64


How to use in the cross case (new)
----------------------------------

Select all binary packages that you want to have as part of the minimal
distribution. This should at least be the Priority:essential packages and
build-essential. Adding debhelper is recommended.

	$ bzcat examples/real/Quantal-amd64-arm64-bootstrap.bz2 | \
	grep-dctrl -X \( \
		-FPackage build-essential --or \
		-FPackage debhelper --or \
		-FEssential yes \
	\) --and \( \
		-FArchitecture arm64 --or \
		-FArchitecture all \
	\) > minimal

To get a quick overview of selected package names, just append

	--no-field-names --show-field=Package

to above command. You can also add Priority:required packages.

Now get a coinstallation set for the selected binary packages. This step makes
sure that your selected binary packages can be installed together and also
outputs all additional binary packages that have to be installed.

	$ ./dose/_build/applications/deb-coinstall.native \
	--bg examples/real/Quantal-amd64-arm64-bootstrap.bz2 \
	--fg minimal --deb-native-arch arm64 -v > minimal-arm64

Get the source packages associated to all binary packages that are not
Architecture:all.

	$ ./bin2src.native --deb-native-arch arm64 minimal-arm64 \
	examples/real/Quantal-Sources-bootstrap.bz2 > minimal-src

Should the above fail because of a Sources.MismatchSrc exception, try adding

	--srcverfallback

to above command.

You can then check which of those source packages already have their cross
build dependencies satisfied, assuming all packages of the host architecture
are available.

	$ ./dose/_build/applications/deb-buildcheck.native \
	--deb-native-arch amd64 --deb-host-arch arm64 \
	examples/real/Quantal-amd64-arm64-bootstrap.bz2 minimal-src \
	--successes

You can also investigate which source packages have unsatisfiable cross build
dependencies by replacing the --successes option with

	--failures --explain

This is helpful for locating conflicts created by missing multiarch
annotations.

Now calculate all arm64 binary packages that must be available to crosscompile
all source packages in minimal-src:

	$ ./build_closure.native -vv \
	--deb-native-arch=amd64 --deb-host-arch=arm64 \
	--bg examples/real/Quantal-Sources-bootstrap.bz2 \
	examples/real/Quantal-amd64-arm64-bootstrap.bz2 \
	minimal-src > minimal-closure

This list can again be turned into a list of source packages using bin2src:

	$ ./bin2src.native --deb-native-arch amd64 minimal-closure \
	examples/real/Quantal-Sources-bootstrap.bz2 > minimal-closure-src

[...]

How to use in the native case (new)
-----------------------------------

Again, select all binary packages for your minimal distribution. This time you
do not have to take care of the architecture.

        $ bzcat examples/real/Quantal-arm64-bootstrap.bz2 | \
        grep-dctrl -X \( \
                -FPackage build-essential --or \
                -FPackage debhelper --or \
                -FEssential yes \
        \) > minimal

Get a coinstallation set:

	$ ./dose/_build/applications/deb-coinstall.native \
        --bg examples/real/Quantal-arm64-bootstrap.bz2 \
        --fg minimal --deb-native-arch arm64 -v > minimal-arm64

Turn it into a list of source packages:

	$ ./bin2src.native --deb-native-arch arm64 minimal-arm64 \
	examples/real/Quantal-Sources-bootstrap.bz2 > minimal-src

Create a reduced distribution using build_closure:

	$ ./build_closure.native -vv --deb-native-arch=arm64 \
	--bg examples/real/Quantal-Sources-bootstrap.bz2 \
	examples/real/Quantal-arm64-bootstrap.bz2 \
	-m minimal-arm64 minimal-src > minimal-closure

Create the list of corresponding source packages:

	$ ./bin2src.native --deb-native-arch arm64 minimal-closure \
	examples/real/Quantal-Sources-bootstrap.bz2 > minimal-closure-src

And check if the selection was done correctly by running deb-buildcheck:

	$ ./dose/_build/applications/deb-buildcheck.native \
	--deb-native-arch=arm64 -vv minimal-closure minimal-closure-src


General options
---------------

 -v         Info messages are printed
 -vv        Info and Debug messages are printed
 --progress If available, progressbars are shown
 --help     Display a usage message
 --timers   If available, timing information is printed before exit
 --quiet    Do not output anything

Debian options
--------------

 --ignore-src-version   Ignores mismatches between versions of binary and
                        source packages
 --deb-native-arch      Native architecture
 --deb-host-arch        Cross compile target architecture
 --deb-foreign-archs    Foreign architectures
 --deb-ignore-essential Ignore Essential packages

How to test
-----------

Only the first time: populate test directory with Packages and Sources files
from snapshot.debian.org

    $ make setup-test

Run tests

    $ make test

Why the name
------------

botch stands for Bootstrap/Build Order Tool CHain.
