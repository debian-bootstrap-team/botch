(**************************************************************************)
(*                                                                        *)
(*  Copyright (C) 2012 Johannes 'josch' Schauer <j.schauer@email.de>      *)
(*  Copyright (C) 2012 Pietro Abate <pietro.abate@pps.jussieu.fr>         *)
(*                                                                        *)
(*  This library is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Lesser General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version.  A special linking    *)
(*  exception to the GNU Lesser General Public License applies to this    *)
(*  library, see the COPYING file for more information.                   *)
(**************************************************************************)

open! ExtLib
open Dose_common
open Dose_debian
open Dose_algo
open Dose_doseparse

#define __label __FILE__
let label =  __label ;;
include Util.Logging(struct let label = label end) ;;

(* 
   given a universe and a set of source packages, build all source
   packages that can be built in that universe, add the newly build
   binary packages to the universe and try to build more source packages
   until no more source packages can be built anymore

   binset : a function that associates sources to binaries
   initialuniverse : the set of available packages
   tocompile : source packages to check

   returns a universe U and a list of binary packages S.
   the universe U composed of all binary packages
   that are generated from the set S
*)
let build_fixpoint ?(global_constraints=[]) binset initialuniverse tocompile =
  let module Set = CudfAdd.Cudf_set in

  let rec aux compiled bin tocompile =
    let univ = Cudf.load_universe ((Set.elements bin)@(Set.elements tocompile)) in
    let ns = CudfAdd.to_set (Depsolver.find_listinstallable ~global_constraints univ (Set.elements tocompile)) in
    if Set.is_empty ns then
        (* return (B_i,C_i) *)
        bin,(List.rev compiled)
    else begin
        (* B_{i+1} = Bin(NS) \cup B_i *)
        let newbin = Set.union (binset (Set.elements ns)) bin in
        (* C_{i+1} = C_i \cup NS *)
        let newcompiled = ns::compiled in
        (* S_{i+1} = S_i \ NS *)
        let newtocompile = Set.diff tocompile ns in
        (* return F C_{i+1} B_{i+1} S_{i+1} *)
        aux newcompiled newbin newtocompile
    end
  in
  aux [] initialuniverse tocompile
;;

module Options = struct
  open OptParse
  let description = (
    "given a list of source packages, return the list of binary packages "^
    "that can be built without having to break a dependency cycle"
  )
  let usage = "%prog [options] --available=AvailablePackages Packages... Sources"

  let options = OptParser.make ~description ~usage
  include BootstrapCommon.MakeOptions(struct let options = options end)

  let available = StdOpt.str_option ()
  let allowsrcmismatch = StdOpt.store_true ()
  let outputorder = StdOpt.str_option ()

  open OptParser ;;

  let prog_group = add_group options "Program specific options" in

  add options ~group:prog_group ~short_name:'A' ~long_name:"available"
    ~help:("List of available packages (arch:all, crossed...) in control file"^
           " format (setting this argument is required)") available;
  add options ~group:prog_group ~long_name:"allowsrcmismatch" ~help:("If a binary package is "^
    "without a source package but there is a source package of same name but "^ 
    "different version, match this binary package to that source package.") allowsrcmismatch;
  add options ~group:prog_group ~long_name:"output-order" ~help:("instead of "^
    "stderr, write the calculated build order to this file") outputorder;

  include StdOptions.InputOptions;;
  let default = List.filter (fun e -> not (List.mem e ["compare"; "checkonly"; "latest"; "inputtype"])) StdOptions.InputOptions.default_options in
  StdOptions.InputOptions.add_options ~default options;;

  include StdOptions.OutputOptions;;
  let default = List.filter (fun e -> not (List.mem e ["outdir"; "dot"])) StdOptions.OutputOptions.default_options in
  StdOptions.OutputOptions.add_options ~default options;;

  include StdOptions.DistribOptions;;
  let default = List.filter (fun e -> not (List.mem e ["deb-profiles"; "deb-ignore-essential"; "deb-builds-from"])) StdOptions.DistribOptions.default_options in
  StdOptions.DistribOptions.add_debian_options ~default options;;

end

let main () =
  let posargs = OptParse.OptParser.parse_argv Options.options in
  StdDebug.enable_debug (OptParse.Opt.get Options.verbose);
  Util.Warning.disable "Depsolver"; (* disable "foo cannot be installed" warnings *)
  Util.Warning.disable "Sources"; (* disable MismatchSrc warnings as exception is caught *)
  StdDebug.all_quiet (OptParse.Opt.get Options.quiet);

  let options = Options.set_deb_options () in
  let hostarch = match options.Debcudf.host with None -> "" | Some s -> s in
  let buildarch = Option.get options.Debcudf.native in
  let foreignarchs = options.Debcudf.foreign in
  let noindep = options.Debcudf.drop_bd_indep in
  let allowsrcmismatch = OptParse.Opt.get Options.allowsrcmismatch in
  
  if not (OptParse.Opt.is_set Options.available) then
    fatal "the --available option must be set";

  let (binlist, (fgsrclist,bgsrclist), origfgsl) = BootstrapCommon.parse_packages ~noindep Options.parse_cmdline buildarch hostarch foreignarchs posargs in

  let tables = Debcudf.init_tables ~options (fgsrclist@bgsrclist@binlist) in
  let global_constraints = Debcudf.get_essential ~options tables in
  let fgsl = List.map (Debcudf.tocudf ?inst:None ~options tables) fgsrclist in
  let bgsl = List.map (Debcudf.tocudf ?inst:None ~options tables) bgsrclist in
  let bl = List.map (Debcudf.tocudf ?inst:None ~options tables) binlist in

  (* create a hashtable mapping package name and version tuples to
   * Sources.source format822 stanzas *)
  let cudftosrc_table = Hashtbl.create 30000 in
  List.iter2 (fun cudfpkg -> fun srcpkg ->
    let id = (cudfpkg.Cudf.package, cudfpkg.Cudf.version) in
    Hashtbl.add cudftosrc_table id srcpkg
  ) fgsl origfgsl;

  let pkglist = BootstrapCommon.unique [fgsl;bgsl;bl] in

  let universe = Cudf.load_universe pkglist in

  (* read package list for available packages *)
  let availableset =
     BootstrapCommon.read_package_file ~archs:(buildarch::hostarch::foreignarchs)
       (Debcudf.tocudf ?inst:None ~options tables) (OptParse.Opt.get Options.available)
  in

  if CudfAdd.Cudf_set.is_empty availableset then
    fatal "the set of available packages must not be empty";

  let binset = Sources.binset (BootstrapCommon.get_bin_packages (BootstrapCommon.srcbin_table ~available:availableset ~allowmismatch:allowsrcmismatch universe)) in
  let tocompile = CudfAdd.to_set fgsl in
  let _, compiled = build_fixpoint ~global_constraints binset availableset tocompile in

  let oc_order =
    if OptParse.Opt.is_set Options.outputorder then
      open_out (OptParse.Opt.get Options.outputorder)
    else
      stderr
  in

  let oc_pkgs =
    if OptParse.Opt.is_set Options.outfile then
      open_out (OptParse.Opt.get Options.outfile)
    else
      stdout
  in

  (* for each of the source packages selected by build-fixpoint, get the
   * associated format822 stanza and print it to stdout *)
  List.iteri (fun i ns ->
      Printf.fprintf oc_order "%d %s\n" (i+1)
        (BootstrapCommon.string_of_list (BootstrapCommon.string_of_package ?noversion:None) ","
           (CudfAdd.Cudf_set.elements ns));
    ) compiled;

  let allcompiled = List.fold_left (fun acc pkgs ->
      CudfAdd.Cudf_set.union acc pkgs
    ) CudfAdd.Cudf_set.empty compiled in

  List.iter (fun p ->
      let id = (p.Cudf.package,p.Cudf.version) in
      let s = Hashtbl.find cudftosrc_table id in
      s#pp oc_pkgs;
    ) (BootstrapCommon.debcudf_sort (CudfAdd.Cudf_set.elements allcompiled));

  close_out oc_order;
  close_out oc_pkgs;
;;

main ();;
